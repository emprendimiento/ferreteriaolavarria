# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-04-01 17:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('apellido', models.CharField(max_length=70, verbose_name='Apellido')),
                ('nombre', models.CharField(max_length=70, verbose_name='Nombre')),
                ('dni_cuit', models.CharField(blank=True, default=b'', max_length=50, verbose_name='DNI/CUIT/CUIL')),
                ('tipo_factura', models.CharField(choices=[('A', 'A'), ('B', 'B'), ('X', 'X')], default=b'A', max_length=100, verbose_name='Tipo de Factura')),
                ('provincia', models.CharField(blank=True, default=b'', max_length=150, verbose_name='Provincia')),
                ('localidad', models.CharField(blank=True, default=b'', max_length=150, verbose_name='Localidad')),
                ('direccion', models.CharField(blank=True, default=b'', max_length=150, verbose_name='Direcci\xf3n')),
                ('codigo_postal', models.IntegerField(blank=True, null=True, verbose_name='Codigo Postal')),
                ('ubicacion', models.CharField(blank=True, default=b'', max_length=150, verbose_name='Mapa')),
                ('email', models.CharField(blank=True, default=b'', max_length=100, verbose_name='E-mail')),
                ('telefono', models.CharField(blank=True, default=b'', max_length=50, verbose_name='Telef\xf3no')),
                ('observaciones', models.TextField(blank=True, default='', verbose_name='Observaciones')),
                ('tipo', models.CharField(choices=[(b'cliente', b'Cliente'), (b'proveedor', b'Proveedor')], default=b'cliente', max_length=64, verbose_name='Tipo')),
                ('cod', models.CharField(blank=True, max_length=5, null=True, verbose_name='Sufijo para Art\xedculos')),
            ],
            options={
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
            },
        ),
        migrations.CreateModel(
            name='ClienteResumen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(blank=True, null=True, verbose_name='Fecha')),
                ('debe', models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Debe')),
                ('haber', models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Haber')),
                ('tipo', models.CharField(choices=[(b'factura', 'Factura'), (b'remito', 'Remito'), (b'compra', 'Compra'), (b'pago', 'Pago'), (b'nota', 'Nota de Credito'), (b'recibo', 'Recibo'), (b'ingreso', 'Ingreso'), (b'egreso', 'Egreso')], max_length=100, verbose_name='Tipo')),
                ('saldo', models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Saldo')),
                ('object_id', models.PositiveIntegerField(verbose_name=b'ID')),
                ('cliente', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cliente.Cliente')),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType', verbose_name='Objeto')),
            ],
            options={
                'verbose_name': 'Resumen',
                'verbose_name_plural': 'Resumen',
            },
        ),
        migrations.CreateModel(
            name='Vendedor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=70, verbose_name='Nombre')),
            ],
            options={
                'verbose_name': 'Vendedor',
                'verbose_name_plural': 'Vendedores',
            },
        ),
        migrations.CreateModel(
            name='CuentaCorriente',
            fields=[
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Cuenta Corriente',
                'proxy': True,
                'verbose_name_plural': 'Cuentas Corrientes',
            },
            bases=('cliente.cliente',),
        ),
        migrations.CreateModel(
            name='Proveedor',
            fields=[
            ],
            options={
                'ordering': ('-pk',),
                'verbose_name': 'Proveedor',
                'proxy': True,
                'verbose_name_plural': 'Proveedores',
            },
            bases=('cliente.cliente',),
        ),
    ]
