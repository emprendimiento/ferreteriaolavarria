# -*- coding: utf-8 -*-
import datetime
from django.contrib import admin
from django import forms
from .clientes import ClienteAdmin
from cliente.models import Proveedor, TipoCliente
from django.forms import ModelForm
from django.forms.widgets import HiddenInput
from cliente.models import Cliente, ProveedorRubro

class ProveedorForm(ModelForm):
    class Meta:
        labels = {
            'apellido': 'Razón Social',
        }
        exclude = []


class ProveedorAdmin(admin.ModelAdmin):
    form = ProveedorForm
    fieldsets = (
        (None, {
            'fields': (('apellido', 'nombre', 'proveedor_cod'),
                       'dni_cuit',
                       'tipo',
                       ('provincia', 'localidad', 'direccion', 'codigo_postal'),
                       'ubicacion',
                       'email',
                       'telefono',
                       'proveedor_rubro',
                       'observaciones'
                       )
        }),
    )
    search_fields = [
        'apellido',
        'nombre',
        'dni_cuit',
        'proveedor_rubro'
    ]
    list_display = [
        'nombre',
        'apellido',
        'dni_cuit',
        'email',
        'telefono',
        'proveedor_rubro'
    ]
    ordering = (
        'apellido',
        'nombre',
    )
    class Media:
        js = (
            'js/ubicaciones.js',
        )

    def get_form(self, request, obj=None, *args, **kwargs):
        form = super(ProveedorAdmin, self).get_form(request, *args, **kwargs)
        form.base_fields['tipo'].initial = TipoCliente.PROVEEDOR
        form.base_fields['tipo'].widget = HiddenInput()
        return form

    class Meta:
        proxy = True

class ProveedorRubroAdmin(admin.ModelAdmin):
    pass

admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(ProveedorRubro, ProveedorRubroAdmin)
