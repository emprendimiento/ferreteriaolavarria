# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import *
from easy_pdf.rendering import render_to_pdf_response
from numbertoletter.number_to_letter import to_word
from configuracion.models import Configuracion, Propietario
from movimientos.models import Movimiento


# Create your views here.
@login_required
def generar_recibo(request, id):
    template_name = "reciboPDF.html"
    recibo = Movimiento.objects.get(pk=id)
    total_texto = to_word(recibo.total, 'ARS')
    conf = Configuracion.objects.all()[0]
    title = str(recibo.numero) + '-' + recibo.cliente.apellido + ', ' + recibo.cliente.nombre
    tipo_mov = str(recibo.tipo_venta).capitalize()
    #cheque_movimiento = ChequeMovimiento.objects.filter(movimiento=recibo)
    prop = Propietario.objects.all()[0]
    return render_to_pdf_response(request, template_name, locals())
