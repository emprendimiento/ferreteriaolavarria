# -*- coding: utf-8 -*-
import datetime
from django.db import models
import select2.fields
import select2.models
from django.db.models import Q
from django.dispatch import receiver
from itertools import chain
from cliente.models import Cliente, ClienteResumen, Vendedor
from articulo.models import Articulo
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete
from smart_selects.db_fields import ChainedForeignKey

from cliente.views import actualizar_cuenta_corriente, crear_cliente_resumen
from configuracion.views import TIPO_FACTURA, TIPO_PAGO, TIPO_VENTA, Tipo, TipoPago


class Tarjeta(models.Model):
    nombre = models.CharField(u'Nombre', max_length=100)
    #recargo = models.DecimalField(u'% Recargo', max_digits=10, decimal_places=2, default=0)

    def __unicode__(self):
        return self.nombre


class Cuota(models.Model):
    tarjeta = models.ForeignKey(Tarjeta, related_name='cuota_tarjeta', on_delete=models.CASCADE)
    cuota = models.IntegerField(u'Cuota', default=0)
    recargo = models.DecimalField(u'% Recargo', max_digits=10, decimal_places=2, default=0.00)

    def __unicode__(self):
        return u'{0}'.format(self.cuota)


class Venta(models.Model):
    numero = models.CharField(u'numero venta', max_length=60, default='')
    punto_venta = models.CharField(u'Punto Venta', max_length=5, default='')
    tipo_venta = models.CharField(u'Tipo', max_length=150, choices=TIPO_VENTA, default=Tipo.PRESUPUESTO)
    fecha = models.DateField(u'Fecha', default=datetime.date.today)
    tipo_factura = models.CharField(u'Tipo de Factura', max_length=100, choices=TIPO_FACTURA, default='X')
    cliente = select2.fields.ForeignKey(Cliente,
                                        ajax=False,
                                        search_field=lambda q: Q(nombre__icontains=q) | Q(apellido__icontains=q) | Q(
                                            dni_cuit__icontains=q),
                                        related_name=u'Cliente',
                                        overlay="...",
                                        js_options={
                                            'quiet_millis': 200,
                                        },
                                        on_delete=models.PROTECT)
    neto_gravado = models.DecimalField(u'NETO GRAVADO', max_digits=20, decimal_places=2, default=0.00)
    iva = models.DecimalField(u'IVA', max_digits=20, decimal_places=2, default=0.00)
    total = models.DecimalField(u'TOTAL', max_digits=20, decimal_places=2, default=0.00)
    forma_pago = models.CharField(u'Forma de Pago', max_length=100, choices=TIPO_PAGO, default='Cuenta Corriente')
    cae = models.CharField(u'CAE', max_length=100, default='No es Factura Digital')
    vencimiento_cae = models.DateField(u'Vencimiento CAE', blank=True, null=True)
    venta_nota = models.ForeignKey('Venta', related_name='asociada', on_delete=models.PROTECT, blank=True, null=True)
    tarjeta = select2.fields.ForeignKey(Tarjeta,
                                        ajax=True,
                                        blank=True, null=True,
                                        search_field=lambda q: Q(nombre__icontains=q),
                                        related_name=u'tarjeta',
                                        overlay="...",
                                        js_options={
                                            'quiet_millis': 200,
                                        },
                                        on_delete=models.PROTECT)
    descuento = models.DecimalField(u'Descuento', max_digits=8, decimal_places=2, default=0.00)
    recargo = models.DecimalField(u'Recargo', max_digits=8, decimal_places=2, default=0.00)
    vendedor = models.ForeignKey(Vendedor, related_name='vendedor_venta', blank=True, null=True)
    cuota = ChainedForeignKey('Cuota',
                                chained_field="tarjeta",
                                chained_model_field="tarjeta",
                                show_all=False,
                                auto_choose=False,
                                blank =True,
                                null =True,
                                )
    percepcion_iibb = models.DecimalField(u'Percepcion ingresos brutos', max_digits=20, decimal_places=2, default=0.00)
    monto_iva21 = models.DecimalField(u'IVA 21', max_digits=20, decimal_places=2, default=0.00)
    monto_iva10 = models.DecimalField(u'IVA 10', max_digits=20, decimal_places=2, default=0.00)
    monto_iva27 = models.DecimalField(u'IVA 27', max_digits=20, decimal_places=2, default=0.00)

    class Meta:
        verbose_name = u'Venta'
        verbose_name_plural = u'Ventas'

    def __unicode__(self):
        if not self.numero == '':
            if self.tipo_venta == Tipo.COMPRA:
                return "{:08d}".format(int(self.numero))
            else:
                return "{:04d}".format(int(self.punto_venta)) + "{:08d}".format(int(self.numero))
        return ''


# --Articulo
class VentaArticulo(models.Model):
    venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
    articulo = select2.fields.ForeignKey(Articulo,
                                         ajax=True,
                                         search_field=lambda q: Q(codigo__icontains=q),
                                         related_name=u'ArticulosVenta',
                                         overlay="...",
                                         js_options={
                                             'quiet_millis': 200,
                                         },
                                         on_delete=models.PROTECT)
    descripcion = models.CharField(u'descripción', max_length=150, default='')
    cantidad = models.DecimalField(u'Cantidad', max_digits=12, decimal_places=2, default=0.00)
    precio = models.DecimalField(u'Precio Unitario', max_digits=20, decimal_places=2, default=0.00)
    iva = models.DecimalField(u'IVA (%)', max_digits=4, decimal_places=2, default=0.00)
    total = models.DecimalField(u'Total', max_digits=20, decimal_places=2, default=0.00)

    class Meta:
        verbose_name = u'Articulo'
        verbose_name_plural = u'Articulo'


@receiver(post_delete, sender=VentaArticulo)
def venta_articulo_post_delete(sender, instance, **kwargs):
    if not instance.venta.tipo_venta == Tipo.COMPRA and not instance.venta.tipo_venta == Tipo.PRESUPUESTO:
        if instance.venta.tipo_venta == Tipo.REMITO:
            art = Articulo.objects.get(id=instance.articulo.id)
            art.stock += instance.cantidad
            art.save()
        elif instance.venta.tipo_venta == Tipo.FACTURA:
            art = Articulo.objects.get(id=instance.articulo.id)
            art.stock += instance.cantidad
            art.save()
        elif instance.venta.tipo_venta == Tipo.NOTA and instance.venta.facturado:
            art = Articulo.objects.get(id=instance.articulo.id)
            art.stock -= instance.cantidad
            art.save()
        # caso compra si llega a ser necesario
        # elif instance.venta.tipo_venta == Tipo.COMPRA:
        #    art = Articulo.objects.get(id=instance.articulo.id)
        #    art.stock -= instance.cantidad


@receiver(pre_save, sender=VentaArticulo)
def venta_articulo_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if VentaArticulo.objects.filter(id=instance.id).exists():
        old_art = VentaArticulo.objects.get(id=instance.id)
        diferencia = old_art.cantidad - instance.cantidad
        if instance.venta.tipo_venta == Tipo.REMITO and not diferencia == 0:
            art = Articulo.objects.get(id=instance.articulo.id)
            art.stock += diferencia
            art.save()
        # caso compra si llega a ser necesario
        # if instance.venta.tipo_venta == Tipo.COMPRA and not diferencia == 0:
        #    art = Articulo.objects.get(id=instance.articulo.id)
        #    art.stock += diferencia
        #    art.save()


# en caso de ser un remito o factura X actualizo el total en el resumen
@receiver(pre_save, sender=Venta)
def venta_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    content_type = ContentType.objects.get(app_label=instance._meta.app_label, model=instance._meta.model_name)
    if instance.tipo_venta == Tipo.REMITO or (instance.tipo_venta == Tipo.FACTURA and instance.tipo_factura == 'X'):
        if ClienteResumen.objects.filter(content_type=content_type, object_id=instance.pk).exists():
            cliente_resumen = ClienteResumen.objects.get(content_type=content_type, object_id=instance.pk)
            cliente_resumen.debe = instance.total
            cliente_resumen.fecha = instance.fecha
            cliente_resumen.save()
            actualizar_cuenta_corriente(instance.cliente)


@receiver(post_delete, sender=Venta)
def venta_post_delete(sender, instance, **kwargs):
    content_type = ContentType.objects.get(app_label=instance._meta.app_label, model=instance._meta.model_name)
    if ClienteResumen.objects.filter(content_type=content_type, object_id=instance.pk).exists():
        cliente_resumen = ClienteResumen.objects.get(content_type=content_type, object_id=instance.pk)
        cliente = cliente_resumen.cliente
        cliente_resumen.delete()
        actualizar_cuenta_corriente(cliente)


class TokenAfip(models.Model):
    token = models.TextField(u'Token')
    sign = models.TextField(u'Sign')
    fecha = models.DateTimeField(u'Fecha')


class CompraManager(models.Manager):
    def get_queryset(self):
        return super(CompraManager, self).get_queryset(). \
            filter(tipo_venta=Tipo.COMPRA)


class Compra(Venta):
    objects = CompraManager()

    def save(self, *args, **kwargs):
        self.tipo_venta = Tipo.COMPRA
        super(Compra, self).save(*args, **kwargs)

    class Meta:
        proxy = True
        ordering = ('-fecha',)
        verbose_name = "Compra"
        verbose_name_plural = "Compras"


@receiver(post_save, sender=Compra)
def compra_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    content_type = ContentType.objects.get(app_label='venta', model='venta')
    if not ClienteResumen.objects.filter(content_type=content_type, object_id=instance.pk).exists():
        if instance.forma_pago == TipoPago.CUENTA_CORRIENTE:
            crear_cliente_resumen(instance, Tipo.COMPRA, instance.total, 0)
        else:
            crear_cliente_resumen(instance, Tipo.COMPRA, instance.total, instance.total)
    else:
        cliente_resumen = ClienteResumen.objects.get(content_type=content_type, object_id=instance.pk)
        if instance.forma_pago == TipoPago.CUENTA_CORRIENTE:
            cliente_resumen.haber = 0
        else:
            cliente_resumen.haber = instance.total
        cliente_resumen.debe = instance.total
        cliente_resumen.save()
        actualizar_cuenta_corriente(instance.cliente)


@receiver(post_delete, sender=Compra)
def compra_post_delete(sender, instance, **kwargs):
    content_type = ContentType.objects.get(app_label='venta', model='venta')
    if ClienteResumen.objects.filter(content_type=content_type, object_id=instance.pk).exists():
        cliente_resumen = ClienteResumen.objects.get(content_type=content_type, object_id=instance.pk)
        cliente = cliente_resumen.cliente
        cliente_resumen.delete()
        actualizar_cuenta_corriente(cliente)


class VentaRemito(models.Model):
    factura = models.ForeignKey(Venta, related_name=u'VentaFactura', )
    remito = models.ForeignKey(Venta, related_name=u'VentaRemito')


class CajaDiariaManager(models.Manager):
    def get_queryset(self):
        ventas = super(CajaDiariaManager, self).get_queryset(). \
            filter(Q(tipo=Tipo.FACTURA) | Q(tipo=Tipo.COMPRA))
        solucion = []
        for v in ventas:
            venta = Venta.objects.get(id=v.object_id)
            if not venta.forma_pago == TipoPago.CUENTA_CORRIENTE:
                solucion.append(v.id)
        ventas = ventas.filter(id__in=solucion)
        movimientos = super(CajaDiariaManager, self).get_queryset(). \
            exclude(Q(tipo=Tipo.FACTURA) | Q(tipo=Tipo.REMITO) | Q(tipo=Tipo.COMPRA)| Q(tipo=Tipo.NOTA))
        return movimientos | ventas


class CajaDiaria(ClienteResumen):
    objects = CajaDiariaManager()

    class Meta:
        proxy = True
        ordering = ('-fecha',)
        verbose_name = "Caja Diaria"
        verbose_name_plural = "Caja Diaria"
