# -*- coding: utf-8 -*-
from decimal import Decimal

import datetime

import select2.fields
import select2.models
from django.db import models

from configuracion.views import CONFIRMACION

# articulos
from django.db.models import Max, Q

from cliente.models import Proveedor


class TipoIva:
    UNO = '21.00'
    DIEZ = '10.50'

TIPO_IVA = (
    (TipoIva.UNO, '21.00'),
    (TipoIva.DIEZ, '10.50'),
    )

def numero_formateado():
    no = Articulo.objects.aggregate(siguiente_numero=Max('id'))
    if no is None:
        return u"ART-{:08d}".format(1)
    if no['siguiente_numero'] is None:
        return u"ART-{:08d}".format(1)
    return u"ART-{:08d}".format(no['siguiente_numero'] + 1)


class ArticuloRubro(models.Model):
    nombre = models.CharField(u'Nombre', max_length=150)

    class Meta:
        verbose_name = u'Rubro'
        verbose_name_plural = u'Rubros'

    def __unicode__(self):
        return u'{0}'.format(self.nombre)


class ArticuloSubRubro(models.Model):
    nombre = models.CharField(u'Nombre', max_length=150)
    rubro = models.ForeignKey(ArticuloRubro, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name = u'Sub rubro'
        verbose_name_plural = u'Sub rubros'

    def __unicode__(self):
        return u'{0}'.format(self.nombre)


class Articulo(models.Model):
    numero = models.CharField(u'numero de articulo', max_length=60, default=numero_formateado)
    codigo = models.CharField(u'Codigo', max_length=50, blank=True, null=True)
    descripcion = models.CharField(u'Descripción', max_length=150, null=False)
    rubro = select2.fields.ForeignKey(ArticuloSubRubro, on_delete=models.SET_NULL, blank=True, null=True)
    codigo_proveedor = models.CharField(u'Codigo Proveedor', max_length=50, blank=True, null=True)
    proveedor = select2.fields.ForeignKey(Proveedor,
                                          ajax=True,
                                          search_field='apellido',
                                          related_name=u'proveedor',
                                          overlay="...",
                                          js_options={
                                              'quiet_millis': 200,
                                          })
    costo = models.DecimalField(u'Costo', max_digits=10, decimal_places=2, default=0.00)
    iva = models.CharField(u'IVA (%)', choices=TIPO_IVA, default=TipoIva.UNO, max_length=5)
    costo_iva = models.DecimalField(u'Costo + IVA', max_digits=20, decimal_places=2, default=0.00)
    ganancia = models.DecimalField(u'Ganancia', max_digits=20, decimal_places=2, default=0.00)
    precio_venta = models.DecimalField(u'Precio de venta', max_digits=20, decimal_places=2, default=0.00)
    stock = models.IntegerField(u'Stock', default=0)
    stock_deposito = models.IntegerField(u'Stock Deposito', default=0)

    class Meta:
        verbose_name_plural = u'Articulos'

    def __unicode__(self):
        return u'{0} '.format(self.codigo)

    def save(self, *args, **kwargs):
        costo = self.costo
        self.costo_iva = Decimal(costo) + (Decimal(costo) * Decimal(self.iva)) / Decimal(100)
        self.precio_venta = (self.costo_iva) + (Decimal(self.costo_iva) * Decimal(self.ganancia) / Decimal(100))
        super(Articulo, self).save(*args, **kwargs)

class Impresion(models.Model):
    fecha = models.DateField(u'Fecha', default=datetime.date.today)
    carga_stock = models.CharField(u'Carga Stock', max_length=2, choices=CONFIRMACION, default='NO')

    class Meta:
        verbose_name = u'Impresion de Etiquetas'
        verbose_name_plural = u'Impresion de Etiquetas'

# --Articulo
class ImpresionArticulo(models.Model):
    impresion = models.ForeignKey(Impresion, on_delete=models.CASCADE)
    articulo = select2.fields.ForeignKey(Articulo,
                                         ajax=True,
                                         search_field=lambda q: Q(codigo__icontains=q),
                                         related_name=u'ArticulosImpresion',
                                         overlay="...",
                                         js_options={
                                             'quiet_millis': 200,
                                         },
                                         on_delete=models.PROTECT)
    cantidad = models.IntegerField(u'Cantidad', default=0)

    def __unicode__(self):
        return u''


