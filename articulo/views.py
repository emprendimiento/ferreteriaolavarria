import csv
from decimal import Decimal

import xlrd as xlrd
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from articulo.forms import formActualizarPrecios
from articulo.models import Articulo
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from easy_pdf.rendering import render_to_pdf_response
from django.template.defaultfilters import *
from articulo.models import ImpresionArticulo
# operacion= True es suma de articulos en stock
from cliente.models import Proveedor
from django.db.models import Q


def stock_articulos(articulos,operacion):
    if operacion:
        for a in articulos:
            articulo = Articulo.objects.get(id=a.articulo.id)
            articulo.stock += a.cantidad
            articulo.save()
    else:
        for a in articulos:
            articulo = Articulo.objects.get(id=a.articulo.id)
            articulo.stock -= a.cantidad
            articulo.save()


# JSONs
@login_required
def proveedor_articulo_json(request, oid):
    if Articulo.objects.filter(codigo_proveedor=oid).exists():
        obj = Articulo.objects.get(codigo_proveedor=oid)
        des = obj.descripcion
        numero = obj.id
    else:
        des = 'El articulo no existe'
        numero = 0
    data = {
        'id': numero,
        'descripcion': des,
    }
    return JsonResponse(data)

@login_required
def all_articulos(request):
    response = JsonResponse(dict(items=list(Articulo.objects.all().values_list('id', 'codigo'))))
    return HttpResponse(response, content_type='application/json')

@login_required
def articulo_json(request, oid):
    obj = Articulo.objects.get(id=oid)
    data = {
        'id': obj.id,
        'precio': obj.precio_venta,
        'iva': obj.iva,
        'nombre': obj.descripcion,
        'descripcion': obj.descripcion,
    }
    return JsonResponse(data)

@register.filter(name='contador')
def contador(cantidad):
    return range(cantidad)

@login_required
def crear_etiquetas(request,id):
    impresiones = ImpresionArticulo.objects.filter(impresion=id)
    lista = []
    sublista = []
    pos = 0
    ancho = 4
    for art in impresiones:
        cant = 0
        while cant < art.cantidad:
            if pos < ancho:
                sublista.append(art.articulo)
                pos += 1
            else:
                lista.append(sublista)
                sublista = []
                sublista.append(art.articulo)
                pos = 1
            cant += 1
    lista.append(sublista)
    sublista = []
    print lista
    title = 'etiquetas'
    pagesize = "A4"
    template_name = "etiquetaPDF.html"
    return render_to_pdf_response(request, template_name, locals())

@login_required
def cargar_articulos(request):
    print ('entro a Cargar Articulos')
    template = "ActualizarPrecios.html"
    form = formActualizarPrecios()
    cant = 0
    row_error = list()
    if request.method == "POST":
        form = formActualizarPrecios(request.POST, request.FILES)
        if form.is_valid():
            id_proveedor = form.cleaned_data['Proveedor']
            prov = Proveedor.objects.get(id=id_proveedor)
            data = request.FILES.get('archivo')

            try:
                book = xlrd.open_workbook(data.name, file_contents=data.read())  # Open book
                xl_sheet = book.sheets()[0]  # Get sheet in index 0
                num_cols = xl_sheet.ncols  # Number of columns
                for row in range(0, xl_sheet.nrows):  # Iterate through rows
                    # print 'Row: %s' % row_idx  # Print row number
                    # for col_idx in range(0, num_cols):  # Iterate through columns
                    codigo = str(xl_sheet.cell(row, 0).value) + '-' + prov.proveedor_cod

                    articulo, created = Articulo.objects.get_or_create(
                        proveedor=prov,
                        codigo_proveedor=xl_sheet.cell(row, 0).value,
                    )

                    if created:
                        articulo.descripcion = xl_sheet.cell(row, 1).value

                    articulo.costo = float(xl_sheet.cell(row, 2).value)
                    articulo.precio_venta = float(xl_sheet.cell(row, 3).value)
                    articulo.codigo = str(xl_sheet.cell(row, 0).value) + '-' + prov.proveedor_cod
                    articulo.save()
                return HttpResponseRedirect("../articulo/articulo")
            except:
                context = {
                    'form': form,
                    'error': 'Archivo no compatible. El archivo debe ser de tipo Excel'
                }
                return render(request, template, context)

    context = {
        'form': form
    }
    return render(request, template, context)

