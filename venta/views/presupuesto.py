# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth.decorators import login_required
from easy_pdf.rendering import render_to_pdf_response
from configuracion.models import Configuracion, Propietario
from venta.models import Venta, VentaArticulo

@login_required
def generar_presupuesto(request, id):
    template_name = "presupuestoPDF.html"
    venta = Venta.objects.get(pk=id)
    #ve = VentaExtendido.objects.get(venta=venta)
    articulos = VentaArticulo.objects.filter(venta=id)
    pagesize = "A4"
    title = venta.numero + '-' + venta.cliente.apellido + ', ' + venta.cliente.nombre
    total = 0
    for v in articulos:
        total += v.articulo.precio_venta * v.cantidad
    prop = Propietario.objects.all()[0]
    conf = Configuracion.objects.all()[0]
    if venta.numero == '':
        conf.presupuesto += 1
        conf.save()
        venta.numero = conf.presupuesto
        venta.punto_venta = 0
        venta.save()
    n = "{:08d}".format(int(venta.numero))
    return render_to_pdf_response(request, template_name, locals())
