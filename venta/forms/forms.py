# -*- coding: utf-8 -*-
# vim:expandtab ts=4 sw=4
from django import forms
from functools import partial

DateInput = partial(forms.DateInput, {'class': 'datepicker form-control vDateField  ', 'id': "fechaI"})
DateInput2 = partial(forms.DateInput, {'class': 'datepicker form-control vDateField ', 'id': "fechaF"})


class formNotaCredito(forms.Form):
    Cliente = forms.IntegerField(widget=forms.Select(attrs={'class': "form-control"}))
    Venta = forms.IntegerField(widget=forms.Select(attrs={'class': "form-control"}))


class formVentas(forms.Form):
    fecha_inicio = forms.DateField(label=u'Fecha Desde', widget=DateInput2())
    fecha_fin = forms.DateField(label=u'Fecha Hasta', widget=DateInput())
    neto_gravado = forms.DecimalField(widget=forms.TextInput(attrs={'class': "form-control vTextField"}))
    iva = forms.DecimalField(max_digits=20, decimal_places=2, widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))
    total = forms.DecimalField(max_digits=20, decimal_places=2, widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))
