# -*- coding: utf-8 -*-
from django.contrib import admin
from django.core import urlresolvers
from librobanco.models import LibroBanco, TipoMovimientoLibroBanco

class AdminLibroBanco(admin.ModelAdmin):
    list_filter = [
        #('fecha_posdata', DateRangeFilter),
        'tipo'
    ]

    list_display = [
        'id',
        'tipo',
        'detalle',
        'monto',
        'fecha',
        'link_a_cheque',
    ]
    ordering = (
        '-fecha',
    )
    search_fields = [
        'cheque__numero',
        'fecha',
        'tipo'
    ]

    def link_a_cheque(self, obj):
        if obj.cheque is not None:
            link = urlresolvers.reverse('admin:cheque_cheque_change', args=[obj.cheque.pk])
            return u'<a href="%s">%s</a>' % (link, obj.cheque.numero)
        else:
            return ""
    link_a_cheque.allow_tags = True

    def save_model(self, request, obj, form, change):
        if(obj.tipo == TipoMovimientoLibroBanco.INGRESO):
            obj.cheque = None
        super(AdminLibroBanco, self).save_model(request, obj, form, change)

admin.site.register(LibroBanco, AdminLibroBanco)
