# -*- coding: utf-8 -*-
from django.contrib import admin

from cheque.models import Cheque, ChequeBanco, ChequeInline, ChequePropioOTerceros
from rangefilter.filter import DateRangeFilter
from django.contrib.contenttypes.admin import GenericTabularInline
from django.db.models import Sum
from librobanco.models import LibroBanco, TipoMovimientoLibroBanco
import datetime

class ChequeInlineAdmin(GenericTabularInline):
    readonly_fields = ['cheque_importe', 'cheque_fecha_posdata']
    verbose_name_plural = u'Cheques'
    verbose_name = u'cheque'
    model = ChequeInline
    extra = 1
    fields = ('cheque','cheque_importe', 'cheque_fecha_posdata')

    def cheque_importe(self, obj):
       return obj.cheque.importe

    def cheque_fecha_posdata(self, obj):
       return obj.cheque.fecha_posdata


class AdminCheque(admin.ModelAdmin):
    readonly_fields = ['numero', 'cobrado']
    actions = ['marcar_cobrado']
    change_list_template = 'admin/custom/change_list_cheque.html'
    list_filter = [
        #('fecha_posdata', DateRangeFilter),
        ('modalidad_de_cheque'),
        ('cobrado')
    ]
    fieldsets = (
        (None, {
            'fields': (
                'numero',
                'banco',
                ('cliente', 'fecha_entrega'),
                'nro_cheque',
                'cuit',
                'importe',
                'fecha_posdata',
                ('entrega', 'fecha_entregado'),
                'descripcion',
                'modalidad_de_cheque',
                'propio_o_tercero',
                'cobrado'
            )
        }),
    )
    list_display = [
        'numero',
        'banco',
        'cliente',
        'nro_cheque',
        'importe',
        'fecha_posdata',
        'fecha_entregado',
        'propio_o_tercero',
        'modalidad_de_cheque',
        'cobrado'
    ]
    ordering = (
        '-fecha_posdata',
    )
    search_fields = [
        'banco__nombre',
        'cliente__apellido',
        'cliente__nombre',
        'fecha_posdata',
        'nro_cheque',
    ]

    def total_cheques(self, request):
        total = Cheque.objects.filter(**request.GET.dict()).aggregate(total=Sum('importe'))['total']
        return total

    def total_cartera(self, request):
        total = Cheque.objects.filter(fecha_entregado__isnull=True, **request.GET.dict()).aggregate(total=Sum('importe'))['total']
        return total

    def changelist_view(self, request, extra_context=None):
        totalCheques = self.total_cheques(request)
        totalCartera = 0 if totalCheques is None else totalCheques
        totalCartera = self.total_cartera(request)
        totalCartera = 0 if totalCartera is None else totalCartera

        contexto = {
            'totalCheques': totalCheques,
            'totalCartera': totalCartera
        }
        return super(AdminCheque, self).changelist_view(request, extra_context=contexto)

    def marcar_cobrado(modeladmin, request, queryset):
        queryset.update(cobrado=True)
        for cheque in queryset:
            existe = LibroBanco.objects.filter(cheque=cheque).exists()
            if not existe:
                detalle = "Cheque cobrado"
                LibroBanco.objects.create(cheque=cheque, detalle=detalle, tipo=TipoMovimientoLibroBanco.EGRESO, monto=cheque.importe, fecha=datetime.date.today())

    marcar_cobrado.short_description = "Marcar como cobrados"

admin.site.register(Cheque, AdminCheque)
admin.site.register(ChequeBanco)
