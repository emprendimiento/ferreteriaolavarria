# -*- coding: utf-8 -*-
import datetime
from django.contrib import admin
from django.contrib.admin import helpers
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.response import TemplateResponse
from easy_pdf.rendering import render_to_pdf_response

from articulo.forms import formActualizarArticulos, formActualizarPrecios
from configuracion.models import Configuracion
from models import Articulo, ArticuloRubro, ArticuloSubRubro, ImpresionArticulo, Impresion



class ArticuloAdmin(admin.ModelAdmin):
    readonly_fields = ['numero', 'costo_iva', 'precio_venta']
    change_list_template = 'admin/change_list_articulo.html'
    actions = ['actualizar', 'listado_de_articulos' ]
    fieldsets = (
        (None, {
            'fields': (
                       ('numero', 'codigo'),
                       ('descripcion','rubro'),
                       ('codigo_proveedor','proveedor'),
                       ('costo','iva', 'costo_iva'),
                       ('ganancia','precio_venta'),
                       ('stock','stock_deposito')
                       )
        }),
    )
    search_fields = [
        'codigo',
        'descripcion',
        'rubro__nombre',
        'precio_venta',

    ]
    list_display = [
        'codigo',
        'descripcion',
        'nombre_prov',
        'rubro',
        #'costo',
        'precio_venta',
    ]
    list_filter = (
        'id',
        'rubro',
        'proveedor',
    )
    ordering = (
        'id',
        'descripcion',
        'rubro',
    )

    def nombre_prov(self, obj):
        if obj.proveedor.nombre:
            return str(obj.proveedor.nombre)
    nombre_prov.short_description = 'Proveedor'

    def actualizar(self, request, queryset):
        t = "actualizarProductos.html"
        if request.POST.get('post'):
            form = formActualizarArticulos(request.POST)
            if form.is_valid():
                aumento = form.cleaned_data['costo']
                iva = form.cleaned_data['iva']
                ganancia = form.cleaned_data['ganancia']
                for q in queryset:
                    if aumento is not None:
                        q.costo += (q.costo / 100) * aumento
                    if iva is not None:
                        q.iva = iva
                    if ganancia is not None:
                        q.ganancia = ganancia
                    q.save()
                self.message_user(request, ("%s." % (u'Los artículos han sido actualizados')))
                return HttpResponseRedirect(request.get_full_path())
        form = formActualizarArticulos()
        context = {
            'title': "Actualizar Articulos",
            'queryset': queryset,
            'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            'form': form,
            'action': 'actualizar',
        }
        return TemplateResponse(request, t, context)

    def actualizarPrecios(self, request):
        if not request.user.is_authenticated():
            return redirect('%s?next=%s' % ('/admin/login', request.path))
        form = formActualizarPrecios()
        context = {
            'form': form,
            'site_header': 'Precios'
        }
        return render(request, 'ActualizarPrecios.html', context )


    def listado_de_articulos( self, request, queryset ):
        template_name = "ListadoArticulosPDF.html"
        articulos = queryset
        fecha = datetime.date.today
        conf = Configuracion.objects.all()[0]
        return render_to_pdf_response(request, template_name, locals())

class ImpresionArticuloAdminInline(admin.TabularInline):
    verbose_name_plural = u'Articulos'
    verbose_name = u'articulo'
    model = ImpresionArticulo
    fieldsets = (
        (None, {
            'fields': (
                ('articulo', 'cantidad'),
            )}),
    )
    extra = 1

class ImpresionAdmin(admin.ModelAdmin):

    fieldsets = (
        (None, {
            'fields': (
                       ('fecha', 'carga_stock'),
                       )
        }),
    )
    search_fields = [
        'id',
    ]
    list_display = [
        'id',
        'fecha',
        'carga_stock',
    ]
    list_filter = (
        'id',
    )
    ordering = (
        'id',
    )
    inlines = [
        ImpresionArticuloAdminInline,
    ]
    
    class Media:
        # EN EL CASO QUE SE NECESITE MOSTRAR LA DESCRIPCION AL LADO DEL CÓDIGO
        #js = (
        #    'js/articulo-impresion.js'
        #)
        css = {
            'all' : ('css/articulo.css',)
        }

class ArticuloSubRubroInlineAdmin(admin.TabularInline):
    verbose_name_plural = u'Sub rubros articulo'
    verbose_name = u'Sub rubro articulo'
    model = ArticuloSubRubro
    extra = 1

class ArticuloRubroAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'nombre',
            )
        }),
    )
    search_fields = [
        'nombre'
    ]
    list_display = [
        'nombre',
    ]
    list_filter = [
        'nombre'
    ]
    inlines = [
        ArticuloSubRubroInlineAdmin,
    ]

admin.site.register(ArticuloRubro, ArticuloRubroAdmin)
admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Impresion, ImpresionAdmin)
