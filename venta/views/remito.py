# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth.decorators import login_required
from easy_pdf.rendering import render_to_pdf_response
from configuracion.models import Configuracion, Propietario
from venta.models import Venta, VentaArticulo
from configuracion.views import Tipo, TipoPago
from django.contrib.contenttypes.models import ContentType
from cliente.views import crear_cliente_resumen, ClienteResumen
from articulo.views import stock_articulos

@login_required
def generar_remito(request, id):
    template_name = "factura-remitoPDF.html"
    venta = Venta.objects.get(pk=id)
    articulos = VentaArticulo.objects.filter(venta=id)
    conf = Configuracion.objects.all()[0]
    content_type = ContentType.objects.get(app_label=venta._meta.app_label, model=venta._meta.model_name)
    if not ClienteResumen.objects.filter(content_type=content_type, object_id=venta.pk).exists():
        crear_cliente_resumen(venta, Tipo.REMITO, venta.total, 0)
        stock_articulos(articulos, False)
        conf.remito += 1
        conf.save()
        venta.forma_pago = TipoPago.CUENTA_CORRIENTE
        venta.numero = conf.remito
        venta.punto_venta = conf.punto_venta
        venta.tipo_venta = Tipo.REMITO
        venta.tipo_factura = 'X'
        venta.save()
    n = "{:08d}".format(int(venta.numero))
    title = str(conf.punto_venta) + '-' + n + '-' + venta.cliente.apellido + ', ' + venta.cliente.nombre
    c = Configuracion.objects.all()
    conf = c[0]
    prop = Propietario.objects.all()[0]
    return render_to_pdf_response(request, template_name, locals())
