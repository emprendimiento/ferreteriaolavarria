# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import *
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from articulo.views import stock_articulos
from cliente.models import ClienteResumen
from cliente.views import crear_cliente_resumen
from configuracion.models import Configuracion
from configuracion.views import Tipo
from venta.models import Compra, VentaArticulo

@login_required
def generar_compra(request, id):
        #template_name = "factura-remitoPDF.html"
        compra = Compra.objects.get(pk=id)
        articulos = VentaArticulo.objects.filter(venta=id)
        conf = Configuracion.objects.all()[0]
        content_type = ContentType.objects.get(app_label=compra._meta.app_label, model= compra._meta.model_name)
        if not ClienteResumen.objects.filter(content_type=content_type, object_id=compra.pk).exists():
            crear_cliente_resumen(compra, Tipo.COMPRA, compra.total, 0)
            stock_articulos(articulos, True)
        return HttpResponseRedirect("../cliente/clientemovimiento/")
