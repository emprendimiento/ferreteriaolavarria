# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django.db.models import Q
from .clientes  import Cliente
from django.utils.translation import ugettext_lazy as _
from cliente_resumen import *

class CuentaCorrienteManager(models.Manager):
    def get_queryset(self):
        #SE AGREGO EL EXCLUDE PORQUE NO SE TIENE QUE MOSTRAR LA CUENTA CORRIENTE DEL CONSUMIDOR FINAL
        # (El cliente tiene que tener el nombre como Consumidor Final)
        return super(CuentaCorrienteManager, self).get_queryset().exclude(nombre='Consumidor Final')
    def create(self, *args, **kwargs):
        kwargs.update()
        return super(CuentaCorrienteManager, self).create(**kwargs)

class CuentaCorriente(Cliente):
    objects = CuentaCorrienteManager()


    class Meta:
        proxy = True
        ordering = ('-pk',)
        verbose_name = _("Cuenta Corriente")
        verbose_name_plural = _("Cuentas Corrientes")

    def cliente_link(self):
        url = "../clienteresumen/?cliente__id__exact=" + str(self.pk)
        return u'<a href="{0}">{1}</a>'.format(url, self.nombre)

    cliente_link.allow_tags = True
    cliente_link.short_description = u'Nombre/Razon Social'
