from django.conf.urls import include, url
from cheque import views

urlpatterns = [
                url(r'^cheque_json/(?P<oid>[0-9]+)/$', views.cheque_json, name='cheque_json'),
                url(r'^cheques_total_json/$', views.cheques_total_json, name='cheques_total_json'),

]
