django.jQuery(function($) {

	function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
	}

	function desactivar_boton(){
		if ($('#parrafo').val() == null){
		$('#generar_movimientos')
		.removeAttr('href')
		.attr('style', ' background-color: #f76f6f; border-color: red' )
		.after('<p id="parrafo" >Primero debe guardar los cambios efectuados</p>');
		}	
	}


	function actualizar_valores_efectivo() {
		var maximo = $('#id_multipleremito_set-TOTAL_FORMS').val();
		total_efectivo = 0;
		for (var x=0; x<maximo; x++) {
			var efectivo = parseFloat($('#id_multipleremito_set-'+x+'-pago_efectivo').val());
			total_efectivo += efectivo;
		}
		var total = total_efectivo - parseFloat($('#id_total_efectivo').val());
		total = roundToTwo(total);
		$("#total-efectivo").val(total);
	}

	function actualizar_valores_cheque() {
		var maximo = $('#id_multipleremito_set-TOTAL_FORMS').val();
		total_cheque = 0;
		for (var x=0; x<maximo; x++) {
			var cheque = parseFloat($('#id_multipleremito_set-'+x+'-pago_cheque').val());
			total_cheque += cheque;
		}
		var total = total_cheque - parseFloat($('#id_total_cheque').val());
		total = roundToTwo(total);
		$("#total-cheque").val(total);
	}


	function solo_lectura(){
		$('#total-efectivo')
		.attr('type', 'number')
		.attr('style', ' background-color: #fff;  border:none ');
		$('#total-cheque')
		.attr('type', 'number')
		.attr('style', ' background-color: #fff;  border:none ');
		$('#id_total')
		.attr('type', 'number')
		.attr('style', ' background-color: #fff; ')
		.attr('readonly','true');
	}

	function movimientos_generados(){
		var mov_gen = $('#id_movimientos_generados').prop('checked');
		if (mov_gen){
			desactivar_boton();
		}
	}
	
	function actualizar_total(){
		var cheque = parseFloat($('#id_total_cheque').val());
		var efectivo =	parseFloat($('#id_total_efectivo').val());
		var total = cheque + efectivo;	
		$('#id_total').val(total);
	}

	$( document ).ready(function() {
		var agregado = '<div><div class="col-sm-2 col-md-offset-8"><label>Resto Efectivo</label><input class="form-control" readonly="readonly" id="total-efectivo"  type="number" value="0.00"></div><div class="col-sm-2"><label>Resto Cheque</label><input class="form-control" readonly="readonly" id="total-cheque"  type="number" value="0.00"></div></br><div>';
		$("#multipleremito_set-group").append(agregado);
		actualizar_valores_efectivo();
		actualizar_valores_cheque();
		solo_lectura();
		movimientos_generados();
	});

	
	$("input[name*='pago_efectivo']").on('change',function(){
		actualizar_valores_efectivo();
		desactivar_boton();
	});

	$("input[name*='pago_cheque']").on('change',function(){
		actualizar_valores_cheque();
		desactivar_boton();
	});

	$("#id_total_efectivo").on('change',function(){
		actualizar_total();
		actualizar_valores_efectivo();
		desactivar_boton();
	});

	$("#id_total_cheque").on('change',function(){
		actualizar_total();
		actualizar_valores_cheque();
		desactivar_boton();
	});

	$("#id_detalle").on('change',function(){
		desactivar_boton();
	});

	




});

