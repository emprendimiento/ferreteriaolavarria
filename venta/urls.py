from django.conf.urls import include, url
from venta import views
from cliente.views import cliente_json

urlpatterns = [url(r"^generarRemito/(\d+)$", views.generar_remito, name='generarRemito'),
               url(r"^generarFactura/(\d+)$", views.generar_factura, name='generarFactura'),
               url(r"^generarPresupuesto/(\d+)$", views.generar_presupuesto, name='generarPresupuesto'),
               url(r"^generarNotaCredito/(\d+)$", views.generarNotaCredito, name='generarNotaCredito'),
              # url(r"^crearVenta/(\d+)$", views.crearVenta, name='crearVenta'),
               url(r"^generarCompra/(\d+)$", views.generar_compra, name='generarCompra'),
               url(r"^pagina_error/(?P<codigo>.+)$", views.pagina_error, name='pagina_error'),
               url(r"^notaCredito/$", views.notaCredito, name='notaCredito'),
               url(r"^ivaventas/$", views.ivaventas, name='ivaventas'),
               url(r"^exportar_facturas/$", views.exportar_facturas, name='exportar_facturas'),
               url(r"^getFacturas/(\d+)$", views.getFacturas, name='getFacturas'),
               url(r'^remito_json/(?P<oid>[0-9]+)/$', views.remito_json, name='remito_json'),
               url(r'^ventas_json/(?P<Iday>\d{1,2})/(?P<Imonth>\d{1,2})/(?P<Iyear>\d{4})/(?P<Fday>\d{1,2})/(?P<Fmonth>\d{1,2})/(?P<Fyear>\d{4})/$', views.ventas_json, name='ventas_json'),
               url(r'^tarjeta_json/(?P<oid>[0-9]+)/$', views.tarjeta_json, name='tarjeta_json'),
               url(r"^caja_diaria_json/(?P<parametro>.+)$", views.caja_diaria_json, name='caja_diaria_json'),
               url(r"^recargo_tarjeta/(?P<nombre>.+)/(?P<cuotas>\d{1,2})/$", views.recargo_tarjeta, name='recargo_tarjeta'),
               url(r"^generar_archivos_contador/$", views.contador_ventas, name='contador_ventas'),
               url(r"^descargar_archivo_contador_ventas", views.descargar_archivo_contador_ventas, name="descargar_archivo_contador_ventas"),
               #url(r'^cliente_json/(?P<oid>[0-9]+)/$', cliente_json, name='cliente_json'),
]
