django.jQuery(function($) {

	function set_valores_cheque( importe, fecha_posdata, fila) {
		var fila_string = String(fila);
		var precio_calculado =  parseFloat(importe);

		$('#cheque-chequeinline-content_type-object_id-'+fila_string+' .field-cheque_importe p').text(precio_calculado);
		$('#cheque-chequeinline-content_type-object_id-'+fila_string+' .field-cheque_fecha_posdata p').text(fecha_posdata);
	}

	$( document ).ready(function() {
        $("input[name*='cheque-chequeinline-content_type-object']").on('change', function (d) {
            var id_name = $(this).context.id;
            var pos = id_name.replace(/[^\d]/g, '');
            var cheque_id = $(this).val();

            if (cheque_id == '')
                return;
            $.ajax({
                url: '/admin/cheque_json/' + cheque_id + '/',
                data: '',
            }).done(function (data) {
                set_valores_cheque(data.importe, data.fecha_posdata, pos);
            });
        });
    });
});
