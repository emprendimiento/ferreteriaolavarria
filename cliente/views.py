import csv

from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, HttpResponseRedirect

from cliente.models import ClienteResumen, Cliente, TipoCliente
from configuracion.views import Tipo
from select2.views import JsonResponse


def crear_cliente_resumen(venta, tipo, debe, haber):
    ct = ContentType.objects.get(app_label=venta._meta.app_label, model=venta._meta.model_name)
    # obligamos a que una compra tenga el content_type de venta
    if tipo==Tipo.COMPRA:
        ct = ContentType.objects.get(app_label='venta', model='venta')
    cr = ClienteResumen()
    cr.cliente = venta.cliente
    cr.fecha = venta.fecha
    cr.tipo = tipo
    cr.debe = debe
    cr.haber = haber
    cr.content_type = ct
    cr.object_id = venta.id
    cr.save()
    actualizar_cuenta_corriente(venta.cliente)


# esta copiado de models
def actualizar_cuenta_corriente(cliente):
    resumen = ClienteResumen.objects.filter(cliente=cliente).exclude(Q(tipo=Tipo.RECIBO) | Q(tipo=Tipo.PAGO) | Q(tipo=Tipo.INGRESO) | Q(tipo=Tipo.EGRESO)).order_by('fecha','id')
    debe = 0
    haber = 0
    for r in resumen:
        debe += r.debe
        haber += r.haber
        r.saldo = debe - haber
        r.save()


# JSONs
@login_required
def all_clientes(request):
    response = JsonResponse(dict(clientes=list(Cliente.objects.filter(tipo=TipoCliente.CLIENTE).values_list('id', 'apellido', 'nombre'))))
    return HttpResponse(response, content_type='application/json')

@login_required
def all_proveedores(request):
    response = JsonResponse(dict(proveedores=list(Cliente.objects.filter(tipo=TipoCliente.PROVEEDOR).values_list('id', 'apellido'))))
    return HttpResponse(response, content_type='application/json')


def cliente_json(request, oid):
    obj = Cliente.objects.get(id=oid)
    data = {
        'id': obj.id,
        'tipo_factura': obj.tipo_factura,
        }
    return JsonResponse(data)

@login_required
def cargar_clientes(request):
    print 'entro a Cargar Clientes'
    with open('listadoClientes.csv', 'rb') as csvFile:
        reader = csv.reader(csvFile, delimiter=';', quotechar='|')
        for row in reader:
            if not Cliente.objects.filter(dni_cuit=row[4]).exists():
                cliente = Cliente()
                cliente.nombre = row[0].decode('latin-1')
                cliente.apellido = "--"
                cliente.provincia = "Buenos Aires"
                cliente.direccion = row[1].decode('latin-1')
                cliente.localidad = row[2].decode('latin-1')
                cliente.dni_cuit = row[4]
                cliente.email = row[6].decode('latin-1')
                cliente.telefono = row[5]
                cliente.save()
            else:
                cliente = Cliente.objects.get(dni_cuit=row[4])
                cliente.provincia = "Buenos Aires"
                cliente.direccion = row[1].decode('latin-1')
                cliente.localidad = row[2].decode('latin-1')
                cliente.save()
    return HttpResponseRedirect("../cliente/cliente")

'''
def crear_movimientos(request, id):
    mulMovimiento = ClienteMultipleMovimiento.objects.get(id=id)
    mulRemito = MultipleRemito.objects.filter(multiple_movimiento=mulMovimiento)
    cheques = None
    conf = Configuracion.objects.filter()[0]
    if ChequeMultipleMovimiento.objects.filter(movimiento=mulMovimiento).exists():
        cheques = ChequeMultipleMovimiento.objects.filter( movimiento=mulMovimiento)
    for remito in mulRemito:
        mov = ClienteMovimiento()
        conf.recibo += 1
        mov.numero = conf.recibo
        mov.cliente = mulMovimiento.cliente
        mov.fecha = mulMovimiento.fecha
        mov.detalle = mulMovimiento.detalle
        mov.venta = remito.remito
        mov.tipo_movimiento = 'Recibo'
        mov.total_efectivo = remito.pago_efectivo
        mov.total_cheque = remito.pago_cheque
        mov.total = remito.pago_efectivo + remito.pago_cheque
        mov.save()
        if cheques is not None:
            for c in cheques:
                cheq = ChequeMovimiento()
                cheq.cheque = c.cheque
                cheq.movimiento = mov
                cheq.fecha_posdata =c.fecha_posdata
                cheq.banco = c.banco
                cheq.importe = c.importe
                cheq.save()
    conf.save()
    return HttpResponseRedirect("../cliente/clientemovimiento/")


def generar_multiples_recibos(request, id):
    template_name = "reciboPDF.html"
    recibo = ClienteMultipleMovimiento.objects.get(pk=id)
    total_texto = to_word(recibo.total, 'ARS')
    title = recibo.cliente.apellido + ', ' + recibo.cliente.nombre
    c = Configuracion.objects.all()
    conf = c[0]
    cheque_movimiento = ChequeMultipleMovimiento.objects.filter(movimiento = recibo)
    return render_to_pdf_response(request, template_name, locals())


# arreglar porque se rompe en MundoVW (multimples movimientos)
def actualizar_valor_resumenes(request):
    clientes = Cliente.objects.all()
    for c in clientes:
        if ClienteResumen.objects.filter(cliente=c).exists():
            resumen = ClienteResumen.objects.filter(cliente=c).order_by('id')
            for r in resumen:
                if r.content_type.model == 'venta':
                    v = 'venta.Venta'.objects.get(id=r.object_id)
                    ve = 'venta.VentaExtendido'.objects.get(venta=v)
                    if v.tipo_venta == "Factura":
                        if ve.forma_pago == "Efectivo":
                            r.debe = ve.total
                            r.haber = ve.total
                        else:
                            r.debe = ve.total
                            r.haber = 0
                    else:
                        r.debe = ve.total
                        r.haber = 0
                else:
                    if ClienteMovimiento.objects.filter(id=r.object_id).exists():
                        m = ClienteMovimiento.objects.get(id=r.object_id)
                        r.debe = 0
                        r.haber = m.total
                r.save()
            actualizar_cuenta_corriente(c)

'''
