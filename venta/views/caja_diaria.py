# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import JsonResponse

from cliente.models import ClienteResumen, Q
from configuracion.views import Tipo, TipoPago
from venta.models import Venta


def parametrizar(parametro):
    parametro = parametro.split('/')
    parametros = parametro[0].split("&")
    d = dict()
    for p in parametros:
        par = p.split('=')
        d.update({par[0]: par[1]})
    return d

@login_required
def caja_diaria_json(request, parametro):
    d = parametrizar(parametro)
    ventas = ClienteResumen.objects.filter(**d)
    ingreso_tarjeta = 0
    ingreso_efectivo = 0
    egreso_tarjeta = 0
    egreso_efectivo = 0

    facturas = ventas.filter(tipo=Tipo.FACTURA)
    for f in facturas:
        venta = Venta.objects.get(id=f.object_id)
        if venta.forma_pago == TipoPago.TARJETA:
            ingreso_tarjeta += float(venta.total)
        elif venta.forma_pago == TipoPago.EFECTIVO:
            ingreso_efectivo += float(venta.total)

    compras = ventas.filter(tipo=Tipo.COMPRA)
    for c in compras:
        venta = Venta.objects.get(id=c.object_id)
        if venta.forma_pago == TipoPago.TARJETA:
            egreso_tarjeta += float(venta.total)
        elif venta.forma_pago == TipoPago.EFECTIVO:
            egreso_efectivo += float(venta.total)

    ingreso = ventas.filter(tipo=Tipo.INGRESO ).aggregate(Sum('haber'))
    ingreso = float(ingreso['haber__sum'] or 0.0)
    ingreso_efectivo += ingreso

    pago = ventas.filter(Q(tipo=Tipo.PAGO)|Q(tipo=Tipo.EGRESO)).aggregate(Sum('debe'))
    pago = float(pago['debe__sum'] or 0.0)
    egreso_efectivo += pago

    recibo = ventas.filter(tipo=Tipo.RECIBO).aggregate(Sum('debe'))
    recibo = float(recibo['debe__sum'] or 0.0)
    ingreso_efectivo += recibo
    data = {
        'ingreso_tarjeta': ingreso_tarjeta,
        'ingreso_efectivo': ingreso_efectivo,
        'egreso_tarjeta': egreso_tarjeta,
        'egreso_efectivo': egreso_efectivo
    }
    return JsonResponse(data)
