# -*- coding: utf-8 -*-
import datetime
from copy import deepcopy
from decimal import Decimal
from django.template.defaultfilters import *
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from easy_pdf.rendering import render_to_pdf_response
from numbertoletter.number_to_letter import to_word
from pyafipws.wsaa import WSAA
from pyafipws.wsfev1 import WSFEv1
from articulo.views import stock_articulos
from cliente.models import ClienteResumen
from cliente.views import crear_cliente_resumen, actualizar_cuenta_corriente
from configuracion.models import Configuracion, Propietario
from venta.forms.forms import formNotaCredito
from venta.models import Venta, VentaArticulo



