django.jQuery(function($) {

	function desactivar_botones(){
		if ($('#parrafo').val() == null){
			$('#generar_remito')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' );
			$('#generar_presupuesto')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' );

			$('#generar_factura')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' )
			.after('<p id="parrafo" >Primero debe guardar los cambios efectuados</p>');
			}
	}

	function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
	}

	function set_valores( precio, iva, descripcion, aclaracion, fila) {
		var fila_string = String(fila);
		var precio_calculado =  0;

		if ($('#id_tipo_factura').val() == 'A') {
			precio_calculado = parseFloat(precio / (1 + iva/100));
		} else {
			precio_calculado = parseFloat(precio);
		}

		$('#id_ventaarticulo_set-'+fila_string+'-iva').val(iva);
		$('#id_ventaarticulo_set-'+fila_string+'-descripcion').val(descripcion);
		$('#id_ventaarticulo_set-'+fila_string+'-precio').val(precio_calculado);
	}

	function actualizar_total() {
		var neto_total = 0;
		var iva_total = 0;
		var max_stock = $('#id_ventaarticulo_set-TOTAL_FORMS').val();
		var precio_row = 0;
		var cantidad = 0;
		var iva = 0;
		var borrar = false;
		var neto_row = 0;
		var iva_row = 0;
		var total_row = 0;
		var tipo_factura = $('#id_tipo_factura').val();
		var percepcion_iibb = parseFloat($('#id_percepcion_iibb').val());
		percepcion_iibb = (isNaN(percepcion_iibb)) ? 0 : percepcion_iibb;

		for (var x=0; x<max_stock; x++) {
			precio_row = parseFloat($('#id_ventaarticulo_set-'+x+'-precio').val());
			cantidad = parseFloat($('#id_ventaarticulo_set-'+x+'-cantidad').val());
			iva = parseFloat($('#id_ventaarticulo_set-'+x+'-iva').val());
			borrar = $("#id_ventaarticulo_set-"+x+"-DELETE").is(':checked');
			neto_row = precio_row * cantidad;
			iva_row = 0;

			if (tipo_factura == 'A') {
				iva_row = neto_row * iva / 100;
			}

			total_row = neto_row + iva_row;
        	if (borrar  == false) {
				neto_total += neto_row;
				iva_total += iva_row;
			}

			iva_row = roundToTwo(iva_row);
			total_row = roundToTwo(total_row);
			$('#id_ventaarticulo_set-'+x+'-costo_iva').val(iva_row);
			$('#id_ventaarticulo_set-'+x+'-total').val(total_row);

		}

		var descuento = $('#id_descuento').val();
		if (descuento > 0){
			neto_total = neto_total - (neto_total * descuento / 100);
			iva_total = iva_total - (iva_total * descuento / 100);
		}

		var recargo = $('#id_recargo').val();
		if (recargo > 0){
			neto_total = neto_total + (neto_total * recargo / 100);
			iva_total = iva_total + (iva_total * recargo / 100);
		}

		var total = neto_total + iva_total + percepcion_iibb;
		neto_total = roundToTwo(neto_total);
		neto_total = (isNaN(neto_total)) ? 'ERROR' : neto_total;

		iva_total = roundToTwo(iva_total);
		iva_total = (isNaN(iva_total)) ? 'ERROR' : iva_total;

		total = roundToTwo(total);
		total = (isNaN(total)) ? 'ERROR' : total;

		// Guardo TOTAL
		$('#id_neto_gravado').val(neto_total);
		$('#id_iva').val(iva_total);
		$('#id_total').val(total);

	}

	$( document ).ready(function() {
		$('#delete_id_cliente').remove();
		$('#delete_id_tarjeta').remove();
		var max_stock = $('#id_ventaarticulo_set-TOTAL_FORMS').val();
		var nuevo = 0;
		var precio = 0;
		var iva = 0;
		var cantidad = 0;
		for (var x=0; x<max_stock;x++) {
			precio = $('#id_ventaarticulo_set-'+x+'-precio').val();
			iva = $('#id_ventaarticulo_set-'+x+'-iva').val();
			cantidad = $('#id_ventaarticulo_set-'+x+'-cantidad').val();
			nuevo = (precio/ (1 + iva/100) * (iva/100));
		}

		// Esto es para la columna de articulos
		$('.table tr th:first-child').prop('colspan', 1);

		setTimeout(function () {
			actualizar_total();
     	}, 500);
	});

	$("#ventaarticulo_set-group input[type='number']").change( function() {
  		var id_name = $(this).context.id;
		var pos = id_name.replace(/[^\d]/g, '');
		var iva = parseFloat($('#id_ventaarticulo_set-'+pos+'-iva').val());
		var precio = parseFloat($('#id_ventaarticulo_set-'+pos+'-precio').val());
		var cantidad = parseFloat($('#'+id_name).val());
		var costo_total_iva = 0;
		var total = precio * cantidad;

		if ($('#id_tipo_factura').val() == 'A') {
			costo_total_iva = cantidad * precio  * (iva / 100);
			total = costo_total_iva + precio * cantidad;
		} else {
			costo_total_iva = 0;
		}

		costo_total_iva = costo_total_iva.toFixed(2);
		$('#id_ventaarticulo_set-'+pos+'-costo_iva').val(costo_total_iva);

		total = total.toFixed(2);
		$('#id_ventaarticulo_set-'+pos+'-total').val(total);

		actualizar_total();
	});

	// seleccionar nuevo articulo
	$("input[name*='ventaarticulo']").on('change',function(d){
		if (d.target.id != 'id_tipo_factura' && !d.target.id.includes('cantidad') && !d.target.id.includes('precio') && !d.target.id.includes('cliente') && !d.target.id.includes('descripcion')) {
			var id_name = $(this).context.id;
			var pos = id_name.replace(/[^\d]/g, '');
			var item_id = $(this).val();
			$('#id_ventaarticulo_set-'+ pos +'-cantidad').val(1);
			if ( item_id == '' )
				return;
			$.ajax({
				url: '/admin/articulo_json/' + item_id + '/',
				data: '',
			}).done(function( data ) {
				set_valores(data.precio, data.iva, data.descripcion, 'articulo', pos);
			});
		}
		setTimeout(function () {
	    		actualizar_total();
	    }, 500);
	 });

	$('#ventaarticulo_set-group .table').on('click', function() {
		actualizar_total();
	});

	(function () {
		var previous;
		$("select").on('focus', function () {
			previous = this.value;
		}).change( function(d) {
			if (d.target.id == 'id_tipo_factura') {
				var max_stock = $('#id_ventaarticulo_set-TOTAL_FORMS').val();
				var precio_row = 0;
				for (var x=0; x<max_stock; x++) {
					iva = 1 + ( parseFloat($('#id_ventaarticulo_set-'+x+'-iva').val()) / 100);
					precio_row = parseFloat($('#id_ventaarticulo_set-'+x+'-precio').val());
					if (previous == 'A'){
						precio_row = precio_row * iva ;
						precio_row = roundToTwo(precio_row);
						$('#id_ventaarticulo_set-'+x+'-precio').val(precio_row);
					}
					if (this.value == 'A'){
						precio_row = precio_row / iva;
						precio_row = roundToTwo(precio_row);
						$('#id_ventaarticulo_set-'+x+'-precio').val(precio_row);
					}
				}
				previous = this.value;
			}
			setTimeout(function () {
				actualizar_total();
			}, 500);
		 });
    })();

	$('a.list-group-item.btn-generar').on('click', function() {
		pathArray = location.href.split( '/' );
		base = pathArray[1] + pathArray[2];
		window.location= 'http://' + base + '/admin/venta/venta';
	});

	function set_factura( tipo_factura ) {

		var $tipo_factura = $('#id_tipo_factura');
		var actual = $.trim($tipo_factura.val());
		var max_stock = $('#id_ventaarticulo_set-TOTAL_FORMS').val();
		var precio_row = 0;

		for (var x=0; x<max_stock; x++) {
			iva = 1 + ( parseFloat($('#id_ventaarticulo_set-'+x+'-iva').val()) / 100);
			precio_row = parseFloat($('#id_ventaarticulo_set-'+x+'-precio').val());
			if (actual == 'A'){
				precio_row = precio_row * iva ;
				precio_row = roundToTwo(precio_row);
				$('#id_ventaarticulo_set-'+x+'-precio').val(precio_row);
			}
			if (tipo_factura == 'A'){
				precio_row = precio_row / iva;
				precio_row = roundToTwo(precio_row);
				$('#id_ventaarticulo_set-'+x+'-precio').val(precio_row);
			}
		}
		$tipo_factura.val( tipo_factura );
	}

	$('#id_percepcion_iibb').on('change', function(){
		actualizar_total();
	});

	$('#id_cliente').on('change', function() {
		var item_id = $(this).val();

		if ( item_id == '' )
			return;
		$.ajax({
			url: '/admin/cliente_json/' + item_id + '/',
			data: '',
		}).done(function( data ) {
			set_factura( data.tipo_factura, 'cliente' );
			actualizar_total();
		});
	});

	$("#id_descuento").on('change',function(){
		actualizar_total();
	});

	$("#id_recargo").on('change',function(){
		actualizar_total();
	});

	$('#id_tarjeta').on('change', function() {
		actualizar_total();
		desactivar_botones();
	});


	function calcularPago(pago) {
		var total = parseFloat($('#id_total').val());

		if (typeof (pago) == 'undefined') {
			pago = parseFloat($('#pago').val());
		}
		var vuelto = pago - total;

		vuelto = roundToTwo(vuelto);

		if (isNaN(vuelto) || total == 0) {
			vuelto = '$0.00';
		} else {
			vuelto = '$ ' + vuelto;
        }

		$("#vuelto").text(vuelto);
    }
	$("#pago").on('change',function(){
		var pago = parseFloat(this.value);
		calcularPago(pago);
	});

	$("#btn-calcular").on('click',function(){
		calcularPago();
	});
});
