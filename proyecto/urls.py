"""proyecto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^select2/', include('select2.urls')),
    url(r'^$', RedirectView.as_view(url='/admin', permanent=True), name='home'),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^admin/', include('articulo.urls')),
    url(r'^admin/', include('cheque.urls')),
    url(r'^admin/', include('cliente.urls')),
    url(r'^admin/', include('configuracion.urls')),
    url(r'^admin/', include('venta.urls')),
    url(r'^admin/', include('movimientos.urls')),
    #url(r'^admin/', include('ivaAfip.urls')),
]
