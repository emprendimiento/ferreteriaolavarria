# -*- coding: utf-8 -*-
from django.contrib import admin
# cliente
from django.http import HttpResponseRedirect
from cliente.models import ClienteResumen, Q
from movimientos.models import Movimiento
from configuracion.views import Tipo
import datetime
from rangefilter.filter import DateRangeFilter
from venta.models import Venta, VentaArticulo, VentaRemito
from configuracion.models import Configuracion, Propietario
from easy_pdf.rendering import render_to_pdf_response
from copy import deepcopy
from django.db.models import Sum

class ClienteResumenAdmin(admin.ModelAdmin):
    change_list_template = 'admin/change_list_cliente_resumen.html'
    list_display_links = None
    search_fields = [
        'cliente__apellido',
        'cliente__nombre',
        'cliente__dni_cuit',
    ]
    list_display = [
        'fecha',
        'tipo',
        'comprobante',
        'debe',
        'haber',
        'saldo',
    ]
    list_filter = [
        'fecha',
        'tipo'
    ]
    actions = ['resumen_de_cuenta', 'crear_recibo', 'crear_pago', 'crear_factura']
    
    def saldo_total(self, request):
        total = ClienteResumen.objects.filter(**request.GET.dict()).aggregate(total=Sum('debe') - Sum('haber'))['total']
        return total

    def get_queryset(self, request):
        return super(ClienteResumenAdmin, self).get_queryset(request).exclude(Q(tipo=Tipo.PAGO) | Q(tipo=Tipo.RECIBO) | Q(tipo=Tipo.EGRESO) | Q(tipo=Tipo.INGRESO))

    def changelist_view(self, request, extra_context=None):
        saldo = self.saldo_total(request)
        saldo = 0 if saldo is None else saldo
        contexto = {
            'total': saldo,
        }
        return super(ClienteResumenAdmin, self).changelist_view(request, extra_context=contexto)

    def has_add_permission(self, request, obj=None):
        return False

    def crear_recibo(self, request, queryset):
        primero = queryset[0]
        if primero.tipo == Tipo.FACTURA or primero.tipo == Tipo.REMITO:
            movimiento = Movimiento()
            movimiento.cliente = primero.cliente
            movimiento.venta = Venta.objects.get(id=primero.object_id)
            movimiento.tipo_venta = Tipo.RECIBO
            movimiento.fecha = datetime.date.today()
            movimiento.total = primero.debe - primero.haber
            movimiento.save()
            return HttpResponseRedirect("../../movimientos/movimiento/"+ str(movimiento.id))


    def crear_pago(self, request, queryset):
        primero = queryset[0]
        if primero.tipo == Tipo.COMPRA:
            movimiento = Movimiento()
            movimiento.cliente = primero.cliente
            movimiento.venta = Venta.objects.get(id=primero.object_id)
            movimiento.tipo_venta = Tipo.PAGO
            movimiento.fecha = datetime.date.today()
            movimiento.total = primero.debe - primero.haber
            movimiento.save()
            return HttpResponseRedirect("../../movimientos/movimiento/"+ str(movimiento.id))

    def crear_factura(self, request, queryset):
        primero = queryset[0]
        ventaP = Venta.objects.get(id=primero.object_id)
        v = Venta()
        v.fecha = datetime.datetime.today()
        v.cliente = ventaP.cliente
        v.id = None
        v.save()
        for q in queryset:
            if q.tipo == Tipo.REMITO:
                if VentaArticulo.objects.filter(venta=q.object_id).exists():
                    articulos = VentaArticulo.objects.filter(venta=q.object_id)
                    for a in articulos:
                        art = deepcopy(a)
                        art.id = None
                        art.venta = v
                        art.save()
                venta = Venta.objects.get(id=q.object_id)
                v.neto_gravado += float(venta.neto_gravado)
                v.iva += float(venta.iva)
                v.total += float(venta.total)
                venta_remito = VentaRemito()
                venta_remito.factura = v
                venta_remito.remito = Venta.objects.get(id=q.object_id)
                venta_remito.save()
        v.save()
        return HttpResponseRedirect("../../venta/venta/" + str(v.id))

    def resumen_de_cuenta( self, request, queryset ):
        template_name = "ResumenCuentaPDF.html"
        cliente = queryset[0].cliente
        resumen_cliente = []
        for q in queryset:
            print q.tipo
            if q.tipo == Tipo.FACTURA or q.tipo == Tipo.REMITO or q.tipo == Tipo.COMPRA:
                v = Venta.objects.get(id=q.object_id)
                m = Movimiento.objects.filter(venta=v)
                resumen_cliente.append((q,v,m))
            else:
                s = Movimiento.objects.get(id=q.object_id)
                pp = [s.detalle]
                resumen_cliente.append((q,s,pp))
        cuenta_cliente = True
        fecha = datetime.date.today
        conf = Configuracion.objects.all()[0]
        return render_to_pdf_response(request, template_name, locals())


admin.site.register(ClienteResumen, ClienteResumenAdmin)
