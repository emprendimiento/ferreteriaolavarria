from __future__ import unicode_literals
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save, post_delete
from cliente.views import actualizar_cuenta_corriente, crear_cliente_resumen
from django.contrib.contenttypes.models import ContentType
from django.dispatch import receiver
from django.apps import apps
import select2.fields
import select2.models
from configuracion.views import TIPO_MOVIMIENTO, Tipo
from cliente.models import Cliente
from smart_selects.db_fields import ChainedForeignKey
from configuracion.models import Configuracion
from django.utils.translation import ugettext_lazy as _
from venta.models import Venta
from cheque.models import Cheque, ChequeInline

class Movimiento(models.Model):
    numero = models.CharField(u'numero de movimiento', max_length=60, default='')
    cliente = select2.fields.ForeignKey(Cliente, ajax=True, search_field=lambda q: Q(nombre__icontains=q) |
                                                                                   Q(apellido__icontains=q),
                                        related_name=u'cliente',
                                        overlay="...",
                                        js_options={'quiet_millis': 200}
                                        )
    venta = ChainedForeignKey('venta.Venta',
                              chained_field="cliente",
                              chained_model_field="cliente",
                              show_all=False,
                              auto_choose=False,
                              blank=True, null=True,
                              on_delete=models.PROTECT
                              )
    tipo_venta = models.CharField(u'Tipo de Movimiento', max_length=50, choices=TIPO_MOVIMIENTO)
    fecha = models.DateField(u'Fecha')
    detalle = models.TextField(u'Detalle', blank=True, default=u'')
    total = models.DecimalField(u'Total', max_digits=20, decimal_places=2, default=0.00)

    class Meta:
        verbose_name = u'Movimiento'
        verbose_name_plural = u'Movimientos'

    def __unicode__(self):
        return self.tipo_venta + "-" + "{:05d}".format(int(self.numero))


# numero de conf, y se usa para modificar los recibos y pagos
@receiver(pre_save, sender=Movimiento)
def movimiento_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if instance.pk is None:
        conf = Configuracion.objects.all()[0]
        if instance.tipo_venta == Tipo.EGRESO:
            conf.egreso += 1
            instance.numero = conf.egreso
        elif instance.tipo_venta == Tipo.INGRESO:
            conf.ingreso += 1
            instance.numero = conf.ingreso
        elif instance.tipo_venta == Tipo.RECIBO:
            conf.recibo += 1
            instance.numero = conf.recibo
        elif instance.tipo_venta == Tipo.PAGO:
            conf.pago += 1
            instance.numero = conf.pago
        conf.save()
        if instance.tipo_venta == Tipo.PAGO or instance.tipo_venta == Tipo.RECIBO:
            resumen = apps.get_model('cliente', 'ClienteResumen')  # esto se usa cuando no podes importar el modelo
            content_type = ContentType.objects.get(app_label=instance.venta._meta.app_label, \
                                                   model=instance.venta._meta.model_name)
            cliente_resumen = resumen.objects.filter(content_type=content_type). \
                get(object_id=instance.venta.id)
            cliente_resumen.haber += instance.total
            cliente_resumen.save()
            actualizar_cuenta_corriente(instance.cliente)
    else:
        if not instance.tipo_venta == Tipo.EGRESO and not instance.tipo_venta == Tipo.INGRESO:
            old = Movimiento.objects.get(id=instance.id)
            if not old.total == instance.total:
                resumen = apps.get_model('cliente', 'ClienteResumen')  # esto se usa cuando no podes importar el modelo
                content_type = ContentType.objects.get(app_label=instance.venta._meta.app_label, \
                                                       model=instance.venta._meta.model_name)
                cliente_resumen = resumen.objects.filter(content_type=content_type). \
                    get(object_id=instance.venta.id)
                cliente_resumen.haber = cliente_resumen.haber - old.total + instance.total
                cliente_resumen.save()
                actualizar_cuenta_corriente(instance.cliente)


# Esto se utiliza para las modificaciones de egresos e ingresos
@receiver(post_save, sender=Movimiento)
def movimiento_post_save(sender, instance, raw, using, update_fields, **kwargs):

    content_type = ContentType.objects.get(app_label=instance._meta.app_label, \
                                           model=instance._meta.model_name)
    resumen = apps.get_model('cliente', 'ClienteResumen')  # esto se usa cuando no podes importar el modelo
    if resumen.objects.filter(content_type=content_type). \
            filter(object_id=instance.id).exists():
        ar = resumen.objects.filter(content_type=content_type). \
            get(object_id=instance.id)
        if instance.tipo_venta == Tipo.INGRESO:
            ar.haber = instance.total
        else:
            ar.debe = instance.total
        ar.fecha = instance.fecha
        ar.cliente = instance.cliente
        ar.save()
        actualizar_cuenta_corriente(instance.cliente)
    else:
        crear_cliente_resumen(instance, instance.tipo_venta, instance.total, 0)

    if instance.tipo_venta == Tipo.PAGO:
        chequesInline = ChequeInline.objects.filter(object_id=instance.id, content_type=ContentType.objects.get_for_model(Movimiento))
        for chequeInline in chequesInline:
            cheque = Cheque.objects.get(id=chequeInline.cheque.id)
            cheque.entrega = instance.cliente.nombre
            cheque.fecha_entregado = instance.fecha
            cheque.save()


@receiver(post_delete, sender=Movimiento)
def movimiento_post_delete(sender, instance, **kwargs):
    resumen = apps.get_model('cliente', 'ClienteResumen')
    if instance.tipo_venta == Tipo.PAGO or instance.tipo_venta == Tipo.RECIBO:
        content_type = ContentType.objects.get(app_label=instance.venta._meta.app_label, \
                                               model=instance.venta._meta.model_name)
        cliente_resumen = resumen.objects.filter(content_type=content_type). \
            get(object_id=instance.venta.id)
        cliente_resumen.haber -= instance.total
        cliente_resumen.save()
    content_type = ContentType.objects.get(app_label=instance._meta.app_label, \
                                               model=instance._meta.model_name)
    cc = resumen.objects.filter(content_type=content_type). \
            get(object_id=instance.id)
    cc.delete()

    if instance.tipo_venta == Tipo.PAGO:
        chequesInline = ChequeInline.objects.filter(object_id=instance.id, content_type=ContentType.objects.get_for_model(Movimiento))
        for chequeInline in chequesInline:
            cheque = Cheque.objects.get(id=chequeInline.cheque.id)
            cheque.entrega = None
            cheque.fecha_entregado = None
            cheque.save()

    actualizar_cuenta_corriente(instance.cliente)


class IngresoEgresoManager(models.Manager):
    def get_queryset(self):
        return super(IngresoEgresoManager, self).get_queryset(). \
            filter(Q(tipo_venta=Tipo.EGRESO) | Q(tipo_venta=Tipo.INGRESO))


class IngresoEgreso(Movimiento):
    objects = IngresoEgresoManager()
    class Meta:
        proxy = True
        ordering = ('-pk',)
        verbose_name = _("Ingreso/Egreso")
        verbose_name_plural = _("Ingresos/Egresos")

    def __unicode__(self):
        return "{:05d}".format(int(self.numero))

    def save(self, *args, **kwargs):
        if Cliente.objects.filter(nombre = "Consumidor Final").exists():
            self.cliente = Cliente.objects.get(nombre = "Consumidor Final")
        else:
            self.cliente = Cliente.objects.get(id = 1)
        super(IngresoEgreso, self).save(*args, **kwargs)


@receiver(pre_save, sender=IngresoEgreso)
def ingresoegreso_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if instance.pk is None:
        conf = Configuracion.objects.all()[0]
        if instance.tipo_venta == Tipo.EGRESO:
            conf.egreso += 1
            instance.numero = conf.egreso
        elif instance.tipo_venta == Tipo.INGRESO:
            conf.ingreso += 1
            instance.numero = conf.ingreso
        conf.save()


@receiver(post_save, sender=IngresoEgreso)
def ingresoegreso_post_save(sender, instance, raw, using, update_fields, **kwargs):
    content_type = ContentType.objects.get(app_label=instance._meta.app_label, \
                                           model=instance._meta.model_name)
    resumen = apps.get_model('cliente', 'ClienteResumen')  # esto se usa cuando no podes importar el modelo
    if resumen.objects.filter(content_type=content_type). \
            filter(object_id=instance.id).exists():
        ar = resumen.objects.filter(content_type=content_type). \
            get(object_id=instance.id)
        if instance.tipo_venta == Tipo.EGRESO:
            ar.debe = instance.total
        else:
            ar.haber = instance.total
        ar.fecha = instance.fecha
        ar.cliente = instance.cliente
        ar.save()
        actualizar_cuenta_corriente(instance.cliente)
    else:
        if instance.tipo_venta == Tipo.EGRESO:
            crear_cliente_resumen(instance, instance.tipo_venta, instance.total, 0)
        elif instance.tipo_venta == Tipo.INGRESO:
            crear_cliente_resumen(instance, instance.tipo_venta, 0, instance.total)


@receiver(post_delete, sender=IngresoEgreso)
def ingresoegreso_post_delete(sender, instance, **kwargs):
    content_type = ContentType.objects.get(app_label=instance._meta.app_label, \
                                           model=instance._meta.model_name)
    resumen = apps.get_model('cliente', 'ClienteResumen')
    if instance.tipo_venta == Tipo.EGRESO or instance.tipo_venta == Tipo.INGRESO:
        cc = resumen.objects.filter(content_type=content_type). \
            get(object_id=instance.id)
        cc.delete()
    actualizar_cuenta_corriente(instance.cliente)
