from django.conf.urls import url
from articulo import views

urlpatterns = [
            url(r"^all_articulos/$", views.all_articulos, name='all_articulos'),
            url(r'^articulo_json/(?P<oid>[0-9]+)/$', views.articulo_json, name='articulo_json'),
            url(r'^proveedor_articulo_json/(?P<oid>[\w\-]+)/$', views.proveedor_articulo_json, name='proveedor_articulo_json'),
            url(r"^crear_etiquetas/(\d+)$", views.crear_etiquetas, name='crear_etiquetas'),
            url(r"^cargar_articulos/$", views.cargar_articulos, name='cargar_articulos'),
    ]
