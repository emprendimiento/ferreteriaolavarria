# -*- coding: utf-8 -*-

from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from cliente.models import TipoCliente
from cheque.admin import ChequeInlineAdmin
from models import *
from rangefilter.filter import DateRangeFilter
from django.forms import ModelForm
from configuracion.views import Tipo

from django.contrib.contenttypes.admin import GenericTabularInline

class VentaArticuloAdminInline(admin.TabularInline):
    verbose_name_plural = u'Articulos'
    verbose_name = u'articulo'
    model = VentaArticulo
    insert_after = 'cliente'
    fieldsets = (
        (None, {
            'fields': (
                ('articulo', 'descripcion', 'cantidad', 'precio', 'iva', 'total'),
            )}),
    )
    extra = 1


class VentaAdmin(admin.ModelAdmin):
    readonly_fields = ['tipo_venta','cae']
    change_list_template = 'admin/change_list_venta.html'
    change_form_template = 'admin/custom/change_form.html'
    actions = None
    fieldsets = (
        (None, {
            'fields': (('tipo_venta',
                        'tipo_factura',
                        'fecha',
                        'vendedor'),
                       )
        }),
        (None, {
            'fields': ('cliente',)
        }),
        (None, {
            'fields': (('forma_pago', 'tarjeta', 'cuota',),),
        }),
        (None, {
            'fields': (('recargo', 'descuento'),),
        }),
        ('Totales', {
            'fields': (('neto_gravado', 'iva', 'total'),)
        }),
        (None, {
            'fields': ('cae',)
        }),

    )
    search_fields = [
        'fecha',
        'vendedor',
        'cliente__nombre',
        'cliente__apellido',
        'cliente__dni_cuit',
    ]
    list_display = [
        'tipo_venta',
        'num',
        'tipo_factura',
        'cliente',
        'fecha',
        'neto_gravado',
        'total',
    ]
    ordering = (
        '-fecha',
        '-numero',
        '-tipo_venta',
    )
    list_filter = ['tipo_venta', 'tipo_factura', ('fecha', DateRangeFilter)]
    inlines = [
        VentaArticuloAdminInline,
        ChequeInlineAdmin,
    ]

    class Media:
        js = (
            'js/ventas.js',
            'js/modificacion-venta.js',
            'js/cheques.js'
        )
        css = {
            'all': ('css/base.css',)
        }

    def num(self, obj):
        if obj.numero:
            return "{:08d}".format(int(obj.numero))

    def get_queryset(self, request):
        return super(VentaAdmin, self).get_queryset(request).exclude(tipo_venta=Tipo.COMPRA)

    def get_form(self, request, obj=None, **kwargs):
        form = super(VentaAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['cliente'].queryset = Cliente.objects.filter(tipo=TipoCliente.CLIENTE)
        return form


class CompraForm(ModelForm):
    class Meta:
        labels = {
            'cliente': 'Proveedor',
            'numero': 'Numero de Factura',

        }
        exclude = ['tarjeta']

    def __init__(self, *args, **kwargs):
        super(CompraForm, self).__init__(*args, **kwargs)
        self.fields['cliente'].queryset = Cliente.objects.filter(tipo=TipoCliente.PROVEEDOR)


class CompraAdmin(admin.ModelAdmin):
    readonly_fields = ['tipo_venta']
    change_list_template = 'admin/change_list_compra.html'
    form = CompraForm
    change_form_template = 'admin/custom/change_form.html'
    fieldsets = (
        (None, {
            'fields': (('tipo_venta',
                        'punto_venta', 'numero',
                        'tipo_factura',
                        'fecha',
                        ),
                       )
        }),
        (None, {
            'fields': ('cliente',)
        }),
        (None, {
            'fields': (('forma_pago', 'recargo', 'descuento'),),
        }),
        ('Totales', {
            'fields': (('neto_gravado', 'iva', 'percepcion_iibb', 'total', ),)
        }),
        ('Totales iva', {
            'fields': (('monto_iva21','monto_iva10','monto_iva27',),)
        })
    )
    list_display = [
        'numero',
        'punto_venta',
        'tipo_factura',
        'cliente',
        'fecha',
        'neto_gravado',
        'total',
    ]
    search_fields = [
        'fecha',
        'cliente__nombre',
        'cliente__apellido',
        'cliente__dni_cuit',
    ]
    ordering = (
        '-fecha',
        '-numero',
        '-tipo_venta',
    )

    class Media:
        js = (
            'js/ventas.js',
            'js/modificacion-venta.js'
        )
        css = {
            'all': ('css/base.css',)
        }
    actions = None
    list_filter = ['tipo_factura', ('fecha', DateRangeFilter)]
    inlines = [
        VentaArticuloAdminInline,
    ]


class CajaDiariaAdmin(admin.ModelAdmin):
    change_list_template = 'admin/change_list_caja_diaria.html'
    search_fields = [
        'cliente__apellido',
        'cliente__nombre',
        'cliente__dni_cuit',
    ]
    list_display = [
        'fecha',
        'tipo',
        'link',
        'monto',
    ]

    def monto(self, obj):
        if obj.tipo == Tipo.INGRESO:
            return obj.haber
        else:
            return obj.debe

    def link(self, obj):
        url = reverse('admin:%s_%s_change' % (obj.content_type.app_label, obj.content_type.model),
                          args=[obj.object_id])
        return format_html(u'<a href="{1}">{0}</a> <br>'.format(obj.content_object, url))

    list_filter = ['tipo', ('fecha', DateRangeFilter)]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request):
        return False


class CuotaAdminInline(admin.TabularInline):
    verbose_name_plural = u'Cuotas'
    verbose_name = u'Cuota'
    model = Cuota
    fieldsets = (
        (None, {
            'fields': (
                ('cuota', 'recargo'),
            )}),
    )
    extra = 0


class TarjetaAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                ('nombre', ),
            )}),
    )
    search_fields = [
        'nombre',
    ]
    ordering = (
        'nombre',
    )
    list_display = [
        'nombre',
    ]
    inlines = [
        CuotaAdminInline,
    ]


class TokenAfipAdmin(admin.ModelAdmin):
    list_display = [
        'fecha',
        'token',
        'sign',
    ]

   

admin.site.register(Tarjeta, TarjetaAdmin)
admin.site.register(Venta, VentaAdmin)
admin.site.register(Compra, CompraAdmin)
admin.site.register(CajaDiaria, CajaDiariaAdmin)
admin.site.register(TokenAfip, TokenAfipAdmin)
