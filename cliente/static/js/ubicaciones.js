django.jQuery(function($) {

    function chequear() {
        if ($("#id_direccion").val() != '') {
            if ($("#id_localidad").val() != '') {
                if ($("#id_provincia").val() != '') {
                    $("#id_ubicacion").val($("#id_direccion").val() + ', ' + $("#id_localidad").val() + ' ('+ $("#id_provincia").val()+ ')');
                } else {
                    // Sin provincia
                    $("#id_ubicacion").val($("#id_direccion").val() + ', ' + $("#id_localidad").val());
                }
            } else {
                //Sin localidad
                if ($("#id_provincia").val() != '') {
                    $("#id_ubicacion").val($("#id_direccion").val() + ', ' + $("#id_provincia").val());    
                } else {
                    //Sin provincia y Sin localidad
                    $("#id_ubicacion").val($("#id_direccion").val());    
                }
            }
        } else {
            //Sin direccion
            if ($("#id_localidad").val() != '') {
                if ($("#id_provincia").val() != '') {
                    $("#id_ubicacion").val($("#id_localidad").val() + ' ('+ $("#id_provincia").val()+ ')');
                } else {
                    $("#id_ubicacion").val($("#id_localidad").val());
                }
            } else {
                // Sin direccion y Sin localidad
                $("#id_ubicacion").val($("#id_provincia").val());
            }
        }
    }

    // Ubicacion solo lectura
    $('#id_ubicacion').attr('readonly', true).attr('style', "display:none");

    $('#id_direccion').on('change', function() {
        chequear();
    });

    $('#id_provincia').on('change', function() {
        chequear();
    });

    $('#id_localidad').on('change', function() { 
        chequear();
    });
});