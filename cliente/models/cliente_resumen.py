# -*- coding: utf-8 -*-
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from cliente.models import Cliente
from configuracion.views import TIPO_NOTIFICACION, Tipo
from django.apps import apps

class ClienteResumen(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, blank=True, null=True)
    fecha = models.DateField(u'Fecha', blank=True, null=True)
    debe = models.DecimalField(u'Debe', max_digits=20, decimal_places=2, default=0.00)
    haber = models.DecimalField(u'Haber', max_digits=20, decimal_places=2, default=0.00)
    tipo = models.CharField(u'Tipo', choices=TIPO_NOTIFICACION, max_length=100)
    saldo = models.DecimalField(u'Saldo', max_digits=20, decimal_places=2, default=0.00)
    limit = models.Q(app_label='venta', model='Venta')
    content_type = models.ForeignKey(ContentType, verbose_name=u'Objeto', limit_choices_to=limit, blank=True, null=True )
    object_id = models.PositiveIntegerField(verbose_name='ID')
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u'Resumen'
        verbose_name_plural = u'Resumen'

    def comprobante(self):
        url = reverse('admin:%s_%s_change' % (self.content_type.app_label, self.content_type.model),
                      args=[self.object_id])
        if self.tipo == Tipo.COMPRA:
                url = reverse('admin:%s_%s_change' % (self.content_type.app_label, 'compra'),
                              args=[self.object_id])

        movimiento = apps.get_model('movimientos','Movimiento')
        links = ''
        if not (self.tipo == Tipo.INGRESO) and not (self.tipo == Tipo.EGRESO):
            if movimiento.objects.filter(venta=self.object_id).exists():
                mov = movimiento.objects.filter(venta=self.object_id)
                for m in mov:
                    url_m = reverse('admin:%s_%s_change' % ('movimientos', 'movimiento'),
                          args=[m.id])
                    links += '<a href="{1}">{0}</a> <br>'.format(m, url_m)
        return u'<a href="{1}">{0}</a> <br>'.format(self.content_object, url) + links

    comprobante.allow_tags = True
