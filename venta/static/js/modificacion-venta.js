django.jQuery(function($) {

	function roundToTwo(num) {
    	return +(Math.round(num + "e+2")  + "e-2");
	}

	function desactivar_botones(){
		if ($('#parrafo').val() == null){
			$('#generar_remito')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' );
			$('#generar_presupuesto')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' );

			$('#generar_factura')
			.removeAttr('href')
			.attr('style', ' background-color: #f76f6f; border-color: red' )
			.after('<p id="parrafo" >Primero debe guardar los cambios efectuados</p>');
			}
	}

	$('select').on('change',function(){
		//desactivar_botones();
	});

	$("input[name*='ventaarticulo']").on('change',function(){
		desactivar_botones();
	});

	$("#id_tipo_factura").on('change',function(){
		desactivar_botones();
	});

	$("#id_fecha").on('change',function(){
		desactivar_botones();
	});

	$("#id_cliente").on('change',function(){
		desactivar_botones();
	});

	$("#id_forma_pago").on('change',function(){
		desactivar_botones();
	});

	$("#id_descuento").on('change',function(){
		desactivar_botones();
	});

	var initialized = false;

	$("#id_cuota").on('change',function(){
		var cuotas = $("#id_cuota option:selected").text();
		var tarjeta = $("#s2id_id_tarjeta span").text();
		if(!isNaN(cuotas)){
			$.ajax({
				url: '/admin/recargo_tarjeta/' + tarjeta + '/' + cuotas +'/',
				data: '',
			}).done(function( data ) {
				if(initialized){
					desactivar_botones();
				}

				initialized = true;
				$("#id_recargo").val(data.recargo);
			});
		}
	});
});
