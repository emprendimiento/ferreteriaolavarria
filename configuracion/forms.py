# -*- coding: utf-8 -*-
# vim:expandtab ts=4 sw=4
from django import forms


class formActualizarPrecios(forms.Form):
    Proveedor = forms.IntegerField(widget=forms.Select(attrs={'class': "form-control"}))
    Nombre = forms.CharField(widget=forms.TextInput(attrs={'class': "form-control vTextField"}))