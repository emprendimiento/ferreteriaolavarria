# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from easy_pdf.rendering import render_to_pdf_response
from configuracion.models import Configuracion, Propietario
from venta.models import Venta, VentaArticulo, Tarjeta, Cuota
from django.template.defaultfilters import *
from django.http import JsonResponse, HttpResponse
from venta.forms.forms import formVentas
import xlwt


# revisar como sabemos que una factura tiene un remito
@register.filter(name='es_remito')
def es_remito(value):
    venta = Venta.objects.get(id=value)
    if venta.tipo_venta == 'Remito':
        return True
    return False


@register.filter(name='es_factura')
def es_factura(value):
    venta = Venta.objects.get(id=value)
    if venta.tipo_venta == 'Factura':
        return True
    return False



@register.filter(name='simbolo_remito')
def existe_remito(value):
    venta = Venta.objects.get(id=value)
    if venta.tipo_venta== 'Remito':
        return 'glyphicon glyphicon-ok'
    else:
        return 'glyphicon glyphicon-remove'


@register.filter(name='Remito')
def remito(value):
    venta = Venta.objects.get(id=value)
    if venta.tipo_venta == 'Remito':
        return 'Nro: {0}'.format(str(venta.numero))
    else:
        return ' '


@register.filter(name='RemitoPDF')
def remitoPDF(value):
    if Venta.objects.filter(factura=value).exists():
        vr = Venta.objects.filter(factura=value)
        remitos = ''
        for r in vr:
            remitos += ' ' + "{:08d}".format(int(r.remito.numero))
        return remitos
    return ''



# JSON
@login_required
def getFacturas(request, id):
    response = JsonResponse(dict(facturas=list(
        Venta.objects.filter(cliente__id=id).filter(tipo_venta=Tipo.FACTURA).values_list('id', 'numero', 'fecha', 'tipo_factura'))))
    return HttpResponse(response, content_type='application/json')

@login_required
def ventas_json(request, Iyear, Imonth, Iday, Fyear, Fmonth, Fday):
    fechaInicio = datetime.datetime(int(Iyear), int(Imonth), int(Iday))
    fechaFin = datetime.datetime(int(Fyear), int(Fmonth), int(Fday))
    print fechaFin
    print fechaInicio
    ventas = Venta.objects.filter(tipo_venta='factura').filter(fecha__range=(fechaInicio, fechaFin)).exclude(tipo_factura='X')
    iva = 0.0
    total = 0
    neto_gravado = 0
    for venta in ventas:
        total += venta.total
        if venta.tipo_factura == 'B':
            neto_gravado = Decimal(neto_gravado) + Decimal(venta.neto_gravado / Decimal(1.21))
            neto_gravado = round(neto_gravado, 2)
            iva = Decimal(iva) + Decimal(venta.total) - (venta.total / Decimal(1.21))
            iva = round(iva, 2)
        else:
            neto_gravado += float(venta.neto_gravado)
            iva = iva + round(venta.iva, 2)
    neto_gravado = round(neto_gravado, 2)
    iva = round(iva, 2)
    data = {
        'iva': iva,
        'total': total,
        'neto': neto_gravado,
    }
    return JsonResponse(data)

@login_required
def remito_json(request, oid):
    obj = Venta.objects.get(venta=oid)
    data = {
        'neto_gravado': obj.neto_gravado,
        'iva': obj.iva,
        'total': obj.total,
    }
    return JsonResponse(data)

@login_required
def tarjeta_json(request, oid):
    obj = Cuota.objects.get(id=oid)
    data = {
        'recargo': obj.recargo,
    }
    return JsonResponse(data)

@login_required
def ivaventas(request):
    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % ('/admin/login', request.path))
    form = formVentas()
    context = {
        'form' : form,
        'site_header': 'Iva Ventas'
    }
    return render(request, 'ivaventas.html', context)

@login_required
def exportar_facturas(request):
    if request.method == "POST":
        form = formVentas(request.POST)
        if form.is_valid():
            fechaI = form.cleaned_data['fecha_inicio']
            fechaF = form.cleaned_data['fecha_fin']
            ventas = Venta.objects.filter(tipo_venta=Tipo.FACTURA, fecha__range=(fechaI, fechaF)).order_by('fecha', 'id').exclude(tipo_factura='X')
            response = HttpResponse(content_type='application/ms-excel')
            response['Content-Disposition'] = 'attachment; filename="Facturas.xls"'
            wb = xlwt.Workbook(encoding='utf-8')
            ws = wb.add_sheet('Users')
            row_num = 0
            font_style = xlwt.XFStyle()
            font_style.font.bold = True
            columns = ['numero', 'tipo Factura', 'fecha', 'cliente', 'neto',
                       'iva', 'total'
                       ]
            for col_num in range(len(columns)):
                ws.write(row_num, col_num, columns[col_num], font_style)
            font_style = xlwt.XFStyle()
            for q in ventas:
                numero = "{:06d}".format(int(q.numero))
                fila = [numero, q.tipo_factura, q.fecha.strftime('%d/%m/%Y'), q.cliente.dni_cuit, q.neto_gravado,
                        q.iva, q.total,
                        ]
                row_num += 1
                for col_num in range(len(fila)):
                    ws.write(row_num, col_num, fila[col_num], font_style)
            wb.save(response)
            return response

