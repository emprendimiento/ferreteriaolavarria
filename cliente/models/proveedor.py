# -*- coding: utf-8 -*-
import datetime
import select2
from django.db import models
from django.db.models import Q
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from .clientes import Cliente, TipoCliente
from .cuenta_corriente import CuentaCorriente

class ProveedorManager(models.Manager):
    def get_queryset(self):
        return super(ProveedorManager, self).get_queryset().\
        filter(tipo=TipoCliente.PROVEEDOR)

    def create(self, *args, **kwargs):
        kwargs.update({'tipo': TipoCliente.PROVEEDOR})
        return super(ProveedorManager, self).create(**kwargs)

class Proveedor(Cliente):
    objects = ProveedorManager()

    def __unicode__(self):
        return u'{0} ({1})'.format(self.apellido, self.dni_cuit)

    class Meta:
        proxy = True
        ordering = ('-pk',)
        verbose_name = _("Proveedor")
        verbose_name_plural = _("Proveedores")
