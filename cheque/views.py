from django.contrib.auth.decorators import login_required

from cheque.models import Cheque
from django.http import JsonResponse
from django.db.models import Sum

# JSON
@login_required
def cheque_json(request, oid):
    obj = Cheque.objects.get(id=oid)
    f = str(obj.fecha_posdata).split('-')
    f = f[2]+'/'+f[1]+'/'+f[0]
    data = {
        'banco': obj.banco.nombre,
        'importe': obj.importe,
        'fecha_posdata': f,
    }
    return JsonResponse(data)

@login_required
def cheques_total_json(request):
    response = Cheque.objects.filter(entrega__exact='').aggregate(Sum('importe'))
    total =  response['importe__sum'] or 0.0
    data = {
        'total' : total,
    }
    return JsonResponse(data)
