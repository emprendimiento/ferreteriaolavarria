# -*- coding: utf-8 -*-
from django.db import models




class Configuracion(models.Model):
    presupuesto = models.IntegerField(u'Presupuesto', default=0)
    facturaX = models.IntegerField(u'Factura X', default=0)
    nota_creditoX = models.IntegerField(u'Nota Credito X', default=0)
    remito = models.IntegerField(u'Remito', default=0)
    recibo = models.IntegerField(u'Recibo', default=0)
    pago = models.IntegerField(u'Pago', default=0)
    ingreso = models.IntegerField(u'Ingreso', default=0)
    egreso = models.IntegerField(u'Egreso', default=0)
    punto_venta = models.IntegerField(u'Punto de Venta',default=0)
    url_wsaa = models.CharField(u' url Conexión AFIP', max_length=250, default="")
    nombre_crt = models.CharField(u'Nombre del CRT', max_length=80, default="")
    nombre_key = models.CharField(u'Nombre de la KEY', max_length=80, default="")
    url_factura = models.CharField(u' url Factura ', max_length=250, default="")
    url_cache_local = models.CharField(u'path absoluto cache', max_length=250, default="")
    url_estaticos = models.CharField(u'path absoluto de fotos', max_length=250, default="")
    web_services = models.TextField(u'detalle de servicios', max_length=500, default="Homologacion WSFEV1"
                                                                                        "WSAA (Conexion AFIP):"
                                                                                        "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"
                                                                                        
                                                                                        "WSFEV1 (FE):"
                                                                                        "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL"
                                                                                        
                                                                                        ""
                                                                                        "Produccion WSEV1:"
                                                                                        ""
                                                                                        "WSAA (Conexion AFIP):"
                                                                                        "https://wsaa.afip.gov.ar/ws/services/LoginCms"
                                                                                        ""
                                                                                        "WSFEV1 (FE):"
                                                                                        "https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL"
                                                                                        "")

    def __unicode__(self):
        return u'Configuracion'

    class Meta:
        verbose_name = u'Configuracion'
        verbose_name_plural = u'Configuraciones'


# crear modelo DUEÑO
class Propietario(models.Model):
    nombre = models.CharField(u'Nombre', max_length=100, default="")
    direccion = models.CharField(u'Dirección', max_length=100, default="")
    telefono = models.CharField(u'Telefono', max_length=100, default="")
    ciudad = models.CharField(u'Ciudad', max_length=100, default="")
    cod_postal = models.IntegerField(u'CP', default=0)
    email = models.CharField(u'Email', max_length=100, default="")
    cuit = models.CharField(u'Cuit (Con guiones)', max_length=100, default="")
    ingresos_brutos = models.CharField(u'I.B.', max_length=100, default="")
    inicio_actividad = models.DateField(u'Inicio de Actividad', max_length=100, default="")
