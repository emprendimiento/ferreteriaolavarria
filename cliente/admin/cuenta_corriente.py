import datetime
from django.contrib import admin
from django.db.models import Q
from django.http import HttpResponseRedirect
from easy_maps.widgets import AddressWithMapWidget
from easy_pdf.rendering import render_to_pdf_response
from reversion.admin import VersionAdmin
from copy import deepcopy
from django import forms
from cliente.models import Cliente, CuentaCorriente, ClienteResumen
from configuracion.models import Configuracion
from configuracion.views import Tipo


class AdminCuentaCorriente(VersionAdmin):
    list_display = [
        'cliente_link',
        'apellido',
        'tipo',
        'saldo',

    ]
    ordering = (
        'nombre',

    )
    search_fields = ['nombre',
                     'apellido',
                     'dni_cuit', ]

    list_display_links = None
    list_filter =['tipo',]
    actions = ['resumen_cuentas']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def saldo(self, obj):
        if ClienteResumen.objects.filter(cliente=obj.pk).exclude(Q(tipo=Tipo.RECIBO) | Q(tipo=Tipo.PAGO) | Q(tipo=Tipo.EGRESO) | Q(tipo=Tipo.INGRESO)).exists():
            return ClienteResumen.objects.filter(cliente=obj.pk).exclude(Q(tipo=Tipo.RECIBO) | Q(tipo=Tipo.PAGO) | Q(tipo=Tipo.EGRESO) | Q(tipo=Tipo.INGRESO)).order_by('-fecha','-id')[:1][0].saldo
        else:
            return 0

    def resumen_cuentas( self, request, queryset ):
        template_name = "ResumenCuentaPDF.html"
        cuentas = queryset
        fecha = datetime.date.today
        total = 0
        lista = []
        for c in cuentas:
            if ClienteResumen.objects.filter(cliente=c).exists():
                saldo = ClienteResumen.objects.filter(cliente=c).order_by('-fecha','-id')[:1][0].saldo
                if saldo > 0:
                    lista.append((c,saldo))
                    total += saldo
        conf = Configuracion.objects.all()[0]
        cuenta_cliente = False
        return render_to_pdf_response(request, template_name, locals())

admin.site.register(CuentaCorriente, AdminCuentaCorriente)
