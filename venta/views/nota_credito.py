import datetime
from copy import deepcopy

from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from easy_pdf.rendering import render_to_pdf_response
from articulo.views import stock_articulos
from cliente.admin import cuenta_corriente
from cliente.models import ClienteResumen, Tipo
from cliente.views import crear_cliente_resumen, actualizar_cuenta_corriente
from configuracion.models import Configuracion, Propietario
from venta.forms.forms import formNotaCredito
from venta.models import Venta, VentaArticulo, TokenAfip
from django.contrib.auth.decorators import login_required



# muestra html de nota de credito en caso de crear nota, copia la venta seleccionada.
from venta.views import digito_verificador_modulo10, crear_factura_electronica

@login_required
def notaCredito(request):
    if request.method == "POST":
        form = formNotaCredito(request.POST)
        if form.is_valid():
            cliente = form.cleaned_data['Cliente']
            ventae = form.cleaned_data['Venta']
            v = Venta.objects.get(id=ventae)
            venta_nueva = deepcopy(v)
            venta_nueva.id = None
            venta_nueva.numero = '0'
            venta_nueva.tipo_venta = Tipo.NOTA
            venta_nueva.fecha = datetime.date.today()
            venta_nueva.venta_nota = Venta.objects.get(id=ventae)
            venta_nueva.save()
            if VentaArticulo.objects.filter(venta=ventae).exists():
                articulos = VentaArticulo.objects.filter(venta=ventae)
                for a in articulos:
                    art = deepcopy(a)
                    art.id = None
                    art.venta = venta_nueva
                    art.save()
            return HttpResponseRedirect("../venta/venta/" + str(venta_nueva.id))
    template = "NotaCredito.html"
    context = {
        'form' : formNotaCredito()
    }
    return render(request, template, context )

@login_required
def generarNotaCredito(request, id):
    template_name = "facturaPDF.html"
    venta = Venta.objects.get(pk=id)
    articulos = VentaArticulo.objects.filter(venta=id)
    datos = None
    conf = Configuracion.objects.all()[0]
    content_type = ContentType.objects.get(app_label=venta._meta.app_label, model=venta._meta.model_name)
    if not ClienteResumen.objects.filter(content_type=content_type).filter(object_id=venta.id).exists():
        if not venta.tipo_factura == 'X':
            datos = crear_factura_electronica(venta, True)
            try:
                venta.cae = datos[1]
                anio = str(datos[0])[:4]
                mes = str(datos[0])[4:-2]
                dia = str(datos[0])[6:]
                venta.vencimiento_cae = anio + '-' + mes + '-' + dia
            except:
                pk = str(datos[2].Errores) + str(datos[2].Observaciones)
                return redirect("pagina_error", codigo=pk)
        venta.save()
        art = VentaArticulo.objects.filter(venta=venta)
        stock_articulos(art, True)
        crear_cliente_resumen(venta, Tipo.NOTA, 0, venta.total)
        if venta.tipo_factura == 'X':
            conf.nota_creditoX += 1
            venta.numero = conf.nota_creditoX
            venta.punto_venta = conf.punto_venta
        else:
            venta.numero = datos[3]
            venta.punto_venta = conf.punto_venta
            if venta.tipo_factura == 'A':
                conf.nota_creditoA = datos[3]
            elif venta.tipo_factura == 'B':
                conf.nota_creditoB = datos[3]
            elif venta.tipo_factura == 'C':
                conf.nota_creditoB = datos[3]
        venta.save()
    prop = Propietario.objects.all()[0]
    pto_venta = "{:04d}".format(conf.punto_venta)
    n = "{:08d}".format(int(venta.numero))
    prop_cuit = prop.cuit.replace("-", "")
    title = pto_venta + str(n) + '-' + venta.cliente.apellido + ', ' + venta.cliente.nombre
    if not venta.tipo_factura=='X':
        fecha = venta.vencimiento_cae
        codigo = prop_cuit + "01" + pto_venta + venta.cae + str(venta.vencimiento_cae).replace("-","")
        codigo = codigo + digito_verificador_modulo10(codigo)
    c = Configuracion.objects.all()[0]
    tipo = venta.tipo_factura
    return render_to_pdf_response(request, template_name, locals())
