# -*- coding: utf-8 -*-
from django.db import models
from configuracion.models import Configuracion
from configuracion.views import TIPO_FACTURA, TIPO_NOTIFICACION, TIPO_MOVIMIENTO


class TipoCliente:
    CLIENTE = 'cliente'
    PROVEEDOR = 'proveedor'

TIPO_CLIENTE = (
    (TipoCliente.CLIENTE, "Cliente"),
    (TipoCliente.PROVEEDOR, "Proveedor"),
    )


def proveedor_cod_unico():
    cant = Cliente.objects.all().order_by('-id')[0].id
    return str(cant).zfill(5)


class ProveedorRubro(models.Model):
    nombre = models.CharField(u'Nombre', max_length=150)

    class Meta:
        verbose_name = u'Rubro'
        verbose_name_plural = u'Rubros proveedor'

    def __unicode__(self):
        return u'{0}'.format(self.nombre)


class Cliente(models.Model):
    apellido = models.CharField(u'Apellido', max_length=70)
    nombre = models.CharField(u'Nombre', max_length=70)
    dni_cuit = models.CharField(u'DNI/CUIT/CUIL', max_length=50, blank=True, default='')
    tipo_factura = models.CharField(u'Tipo de Factura', max_length=100, choices=TIPO_FACTURA, default='A')
    provincia = models.CharField(u'Provincia', max_length=150, blank=True, default='')
    localidad = models.CharField(u'Localidad', max_length=150, blank=True, default='')
    direccion = models.CharField(u'Dirección', max_length=150, blank=True, default='')
    codigo_postal = models.IntegerField(u'Codigo Postal', blank=True, null=True)
    ubicacion = models.CharField(u'Mapa', max_length=150, blank=True, default='')
    email = models.CharField(u'E-mail', max_length=100, blank=True, default='')
    telefono = models.CharField(u'Telefóno', max_length=50, blank=True, default='')
    observaciones = models.TextField(u'Observaciones', blank=True, default=u'')
    tipo = models.CharField(u'Tipo', max_length=64, choices=TIPO_CLIENTE, default=TipoCliente.CLIENTE)
    proveedor_cod = models.CharField(u'Sufijo para Artículos', max_length=5, default=proveedor_cod_unico)
    proveedor_rubro = models.ForeignKey(ProveedorRubro, on_delete=models.SET_NULL, blank=True, null=True)

    def __unicode__(self):
        if self.tipo == TipoCliente.CLIENTE:
            return u'{0}, {1} ({2})'.format(self.apellido, self.nombre, self.dni_cuit)
        else:
            return u'{0} ({1})'.format(self.apellido, self.dni_cuit)

    class Meta:
        verbose_name = u'Cliente'
        verbose_name_plural = u'Clientes'


class Vendedor(models.Model):
    nombre = models.CharField(u'Nombre', max_length=70)

    def __unicode__(self):
        return u'{0}'.format(self.nombre)

    class Meta:
        verbose_name = u'Vendedor'
        verbose_name_plural = u'Vendedores'

# esta aca y en views
'''
def actualizar_cuenta_corriente(cliente):
    cuenta_corriente = CuentaCorriente.objects.get(cliente=cliente)
    resumen = ClienteResumen.objects.filter(cliente=cliente).order_by('fecha','id')
    debe = 0
    haber = 0
    for r in resumen:
        debe += r.debe
        haber += r.haber
        r.saldo = debe - haber
        r.save()
    cuenta_corriente.haber = haber
    cuenta_corriente.deuda = debe
    cuenta_corriente.saldo = debe - haber
    cuenta_corriente.save()
'''
