# -*- coding: utf-8 -*-
import datetime

from django import forms

MONTH_CHOICES = [(r, r) for r in range(1, 13)]

TIPO_ARCHIVO = (
       (u'1', u'Ventas'),
       (u'2', u'Alicuota Ventas'),
       (u'3', u'Compras'),
       (u'4', u'Alicuota Compras'),
)


class CrearArchivosContadorForm(forms.Form):
    mes = forms.ChoiceField(label=u'Mes', choices=MONTH_CHOICES, initial=datetime.datetime.now().month, widget=forms.Select(attrs={'class': "form-control"}))
    anio = forms.IntegerField(label=u'Año', widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))
    tipo_archivo = forms.ChoiceField(label=u'Tipo de Archivo', choices=TIPO_ARCHIVO, widget=forms.Select(attrs={'class': "form-control"}))