# -*- coding: utf-8 -*-
from .settings import *
DEBUG = True
TEMPLATE = {
    'DEBUG': DEBUG,
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
