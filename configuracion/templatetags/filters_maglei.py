from django import template
from django.utils.encoding import force_unicode

register = template.Library()

@register.filter
def in_group(user, groups):
    """Returns a boolean if the user is in the given group, or comma-separated
    list of groups.

    Usage::

        {% if user|in_group:"Friends" %}
        ...
        {% endif %}

    or::

        {% if user|in_group:"Friends,Enemies" %}
        ...
        {% endif %}

    """
    group_list = force_unicode(groups).split(',')
    return bool(user.groups.filter(name__in=group_list).values('name'))


@register.filter
def nice_name(user):
    """
    Example::

    Hi, {{ user|nice_name }}
    """
    if user.last_name == '' and user.first_name == '':
        return user.username
    if user.last_name != '' and user.first_name != '':
        return u'{0}, {1}'.format(user.last_name, user.first_name)
    if user.last_name == '':
        return user.first_name
    return user.last_name

