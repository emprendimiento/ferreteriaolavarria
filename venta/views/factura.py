# -*- coding: utf-8 -*-
import datetime
from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import *
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from easy_pdf.rendering import render_to_pdf_response
from numbertoletter.number_to_letter import to_word
from pyafipws.wsaa import WSAA
from pyafipws.wsfev1 import WSFEv1

from articulo.models import TipoIva
from articulo.views import stock_articulos
from cliente.models import ClienteResumen
from cliente.views import crear_cliente_resumen, actualizar_cuenta_corriente
from configuracion.models import Configuracion, Propietario
from configuracion.views import Tipo, TipoPago
from venta.models import Venta, VentaArticulo, TokenAfip


def digito_verificador_modulo10(codigo):
    "Rutina para el cálculo del dígito verificador 'módulo 10'"
    # http://www.consejo.org.ar/Bib_elect/diciembre04_CT/documentos/rafip1702.htm
    # Etapa 1: comenzar desde la izquierda, sumar todos los caracteres ubicados en las posiciones impares.
    codigo = codigo.strip()
    if not codigo or not codigo.isdigit():
        return ''
    etapa1 = sum([int(c) for i, c in enumerate(codigo) if not i % 2])
    # Etapa 2: multiplicar la suma obtenida en la etapa 1 por el número 3
    etapa2 = etapa1 * 3
    # Etapa 3: comenzar desde la izquierda, sumar todos los caracteres que están ubicados en las posiciones pares.
    etapa3 = sum([int(c) for i, c in enumerate(codigo) if i % 2])
    # Etapa 4: sumar los resultados obtenidos en las etapas 2 y 3.
    etapa4 = etapa2 + etapa3
    # Etapa 5: buscar el menor número que sumado al resultado obtenido en la etapa 4 dé un número múltiplo de 10. Este será el valor del dígito verificador del módulo 10.
    digito = 10 - (etapa4 - (int(etapa4 / 10) * 10))
    if digito == 10:
        digito = 0
    return str(digito)


def wsaaConexion():
    c = Configuracion.objects.all()
    conf = c[0]
    WSDL_LOGIN = conf.url_wsaa
    CACHE = conf.url_cache_local
    CERT3 = conf.nombre_crt#'certificado.crt'
    PRIVATEKEY3 = conf.nombre_key#'llave.key'
    wsaa = WSAA()
    tra = wsaa.CreateTRA(service="wsfe")
    cms = wsaa.SignTRA(tra, CERT3, PRIVATEKEY3)
    wsaa.Conectar(CACHE, WSDL_LOGIN)
    wsaa.LoginCMS(cms)
    return wsaa


def guardarToken():
    wsaa = wsaaConexion()
    token = TokenAfip.objects.all()[0]
    token.sign = wsaa.Sign
    token.token = wsaa.Token
    token.fecha = datetime.datetime.now()
    token.save()

def conexionAfip():
    if TokenAfip.objects.all().exists():
        token = TokenAfip.objects.all()[0]
        hora_futuro = token.fecha + datetime.timedelta(hours=6)
        if token.fecha.day < datetime.datetime.today().day:
            guardarToken()
        elif hora_futuro.day == datetime.datetime.today().day and hora_futuro.time < datetime.datetime.now().time:
            guardarToken()
    else:
        # primera vez crea el token
        wsaa = wsaaConexion()
        token = TokenAfip()
        token.sign = wsaa.Sign
        token.token = wsaa.Token
        token.fecha = datetime.datetime.now()
        token.save()


def getTipoFactura(tipo_factura, nota):
    # tipo de comprobante Factura A = 1 y  Factura B = 6 y Factura C = 11
    # tipo de comprobante Nota de Credito A = 3 y  B = 8 y Factura C = 13
    if nota:
        if tipo_factura == 'A':
            return 3
        elif tipo_factura == 'B':
            return 8
        elif tipo_factura == 'C':
            return 13
    else:
        if tipo_factura == 'A':
            return 1
        elif tipo_factura == 'B':
            return 6
        elif tipo_factura == 'C':
            return 11

def calcularIVAs(articulos, wsfev1, tipo_factura):
    iva10 = 0
    base_imp10 = 0
    iva21 = 0
    base_imp21 = 0

    for a in articulos:
        total = a.total + a.total * a.venta.recargo/100 - a.total * a.venta.descuento/100
        costo_iva = (a.precio + a.precio * a.venta.recargo/100 - a.precio * a.venta.descuento/100) * a.cantidad * (Decimal(a.iva) / 100)
        if str(a.iva) == TipoIva.UNO:
            iva21 += costo_iva
            base_imp21 += total - costo_iva
        elif str(a.iva) == TipoIva.DIEZ:
            iva10 += costo_iva
            base_imp10 += total - costo_iva
    if iva10 > 0:
        base_imp10 = round(base_imp10, 2)
        iva10 = round(iva10, 2)
        if tipo_factura == "A":
            wsfev1.AgregarIva(4, base_imp10, iva10)
    if iva21 > 0:
        base_imp21 = round(base_imp21, 2)
        iva21 = round(iva21, 2)
        if tipo_factura == "A":
            wsfev1.AgregarIva(5, base_imp21, iva21)

    return [iva21, iva10, 0]

def crear_factura_electronica(venta, nota):
    conf = Configuracion.objects.all()[0]
    prop = Propietario.objects.all()[0]
    prop_cuit = prop.cuit.replace("-", "")
    # conexiones
    conexionAfip()
    CACHE = conf.url_cache_local
    WSDL = conf.url_factura
    wsfev1 = WSFEv1()
    wsaa = TokenAfip.objects.all()[0]
    wsfev1.Token = wsaa.token
    wsfev1.Sign = wsaa.sign
    wsfev1.Cuit = prop_cuit
    wsfev1.Conectar(CACHE, WSDL)
    # Datos de factura  1 productos
    concepto = 1
    tipo_cbte = getTipoFactura(venta.tipo_factura, nota)
    imp_neto = venta.neto_gravado
    imp_op_ex = "0.00"
    if venta.tipo_factura == "B":
        imp_op_ex = venta.neto_gravado
        imp_neto = 0
    punto_vta = conf.punto_venta
    cbte_nro = long(wsfev1.CompUltimoAutorizado(tipo_cbte, punto_vta) or 0)
    cbte_nro = long(cbte_nro) + 1
    f = venta.fecha  # fecha de la factura
    cuit = venta.cliente.dni_cuit
    if len(cuit) <= 8:
        nro_doc = cuit
        tipo_doc = 96 #96 es documento
    else:
        tipo_doc = 80  # 80 es cuit
        nro_doc = cuit.replace("-", "")
    cbt_desde = cbte_nro
    cbt_hasta = cbt_desde
    if f.month <= 9:
        mes = '0' + str(f.month)
    else:
        mes = str(f.month)
    if f.day <= 9:
        dia = '0' + str(f.day)
    else:
        dia = str(f.day)
    fecha = str(f.year) + mes + dia
    fecha_cbte = fecha
    fecha_venc_pago = fecha_serv_desde = fecha_serv_hasta = None  # tiene una sola fecha
    moneda_id = 'PES'  # moneda
    moneda_ctz = '1.00'  # cotizacion de moneda si es pesos vale 1
    # valores de la factura
    imp_tot_conc = "0.00"  # importeNoGravado
    imp_trib = "0.00"  # importe otros tributos
    imp_iva = Decimal(venta.iva)
    imp_total = venta.total  # IMPORTE TOTAL
    wsfev1.CrearFactura(concepto, tipo_doc, nro_doc,
                        tipo_cbte, punto_vta, cbt_desde, cbt_hasta,
                        imp_total, imp_tot_conc, imp_neto,
                        imp_iva, imp_trib, imp_op_ex, fecha_cbte, fecha_venc_pago,
                        fecha_serv_desde, fecha_serv_hasta,  # --
                        moneda_id, moneda_ctz)

    articulos = VentaArticulo.objects.filter(venta=venta)
    ivas = calcularIVAs(articulos, wsfev1, venta.tipo_factura)

    # Se obtiene el la instancia de objeto venta y se actualizan los montos de iva 10, 21 y 27
    instancia_obj_venta = Venta.objects.get(id=venta.id)
    instancia_obj_venta.monto_iva21 = ivas[0]
    instancia_obj_venta.monto_iva10 = ivas[1]
    instancia_obj_venta.monto_iva27 = ivas[2]
    instancia_obj_venta.save()

    wsfev1.CAESolicitar()
    diccionario = [wsfev1.Vencimiento, wsfev1.CAE, wsfev1, cbte_nro]
    return diccionario

@login_required
def pagina_error(request, codigo):
    cod=codigo
    template = "pagina_error.html"
    context = {
        'cod' : cod,
    }
    return render(request, template, context)

@login_required
def generar_factura(request, id):
    venta = Venta.objects.get(pk=id)
    template_name = "facturaPDF.html"
    articulos = VentaArticulo.objects.filter(venta=id)
    datos = None
    content_type = ContentType.objects.get(app_label=venta._meta.app_label, model=venta._meta.model_name)
    if not ClienteResumen.objects.filter(content_type=content_type).filter(object_id=venta.id).exists():
        if not venta.tipo_factura == 'X':
            datos = crear_factura_electronica(venta, False)
            print 'Observaciones:'
            print str(datos[2].Observaciones)
            print datos[1]
            if not len(str(datos[1])) > 2:
                pk = str(datos[2].Errores) + str(datos[2].Observaciones)
                return redirect("pagina_error", codigo=pk)
            else:
                try:
                    venta.cae = datos[1]
                    anio = str(datos[0])[:4]
                    mes = str(datos[0])[4:-2]
                    dia = str(datos[0])[6:]
                    venta.vencimiento_cae = anio + '-' + mes + '-' + dia
                    cae = datos[1]
                except:
                    pk = str(datos[2].Errores) + str(datos[2].Observaciones)
                    return redirect("pagina_error", codigo=pk)

        if venta.forma_pago == TipoPago.CUENTA_CORRIENTE:
            crear_cliente_resumen(venta, Tipo.FACTURA, venta.total, 0)
        else:
            crear_cliente_resumen(venta, Tipo.FACTURA, venta.total, venta.total)
        stock_articulos(articulos, False)
        venta.tipo_venta = Tipo.FACTURA
        conf = Configuracion.objects.all()[0]
        if venta.tipo_factura == 'X':
            conf.facturaX += 1
            venta.numero = conf.facturaX
        else:
            venta.numero = datos[3]
        venta.punto_venta = conf.punto_venta
        venta.save()
        conf.save()
    venta = Venta.objects.get(pk=id)
    c = Configuracion.objects.all()
    conf = c[0]
    pto_venta = "{:04d}".format(conf.punto_venta)
    prop = Propietario.objects.all()[0]
    prop_cuit = prop.cuit.replace("-", "")
    if not venta.tipo_factura == 'X' and not venta.tipo_factura == 'C':
        cod = prop_cuit + "01" + pto_venta + venta.cae + str(venta.vencimiento_cae).replace("-", "")
        codigo = cod + digito_verificador_modulo10(cod)
    n = "{:08d}".format(int(venta.numero))
    title = pto_venta + str(n) + '-' + venta.cliente.apellido + ', ' + venta.cliente.nombre
    return render_to_pdf_response(request, template_name, locals())
