# -*- coding: utf-8 -*-
# vim:expandtab ts=4 sw=4
from django import forms


class formActualizarArticulos(forms.Form):
    costo = forms.DecimalField(label=u'Costo %', required=False, max_digits=20, decimal_places=2,
                               widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))
    iva = forms.DecimalField(label=u'Iva %', required=False, max_digits=20, decimal_places=2,
                             widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))
    ganancia = forms.DecimalField(label=u'Ganancia %', required=False, max_digits=20, decimal_places=2,
                                  widget=forms.NumberInput(attrs={'class': "form-control vTextField"}))


class formActualizarPrecios(forms.Form):
    Proveedor = forms.IntegerField(widget=forms.Select(attrs={'class': "form-control"}))
    archivo = forms.FileField()

