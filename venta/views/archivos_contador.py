# -*- coding: utf-8 -*-
from decimal import Decimal

from django.contrib.auth.decorators import login_required

from venta.models import Compra, Venta, VentaArticulo

from django.db.models import Q
from venta.forms.crear_archivos_contador import CrearArchivosContadorForm
from django.shortcuts import render
from django.http import HttpResponse
from configuracion.views import Tipo

@login_required
def contador_ventas(request):
    form = CrearArchivosContadorForm()
    context = {
        'form': form,
        'site_header': 'Contador Ventas ',
    }
    return render(request, 'archivos_contador.html', context)

# contador
def tipo_factura_afip(venta, tipo_venta, tipo_factura):
    if tipo_venta == 'Factura':
        if tipo_factura == 'A':
            numero = "{:020d}".format((int)(venta.numero))
            return ['001', numero]
        else:
            numero = "{:020d}".format((int)(venta.numero))
            return ['006', numero]
    else:
        if tipo_factura == 'A':
            numero = "{:020d}".format((int)(venta.numero))
            return ['003', numero]
        else:
            if venta.numero != '':
                numero = "{:020d}".format((int)(venta.numero))
                return ['008', numero]
            else:
                return ['008', 'xxxx']

@login_required
def descargar_archivo_contador_ventas(request):
    form = CrearArchivosContadorForm(request.POST)
    tipo = None
    if form.is_valid:
        tipo = form.data['tipo_archivo']
    response = None
    if form.is_valid():
        if tipo == '1':
            response = crear_archivo_iva_ventas(form)
        elif tipo == '2':
            response = crear_archivo_alicuotas_ventas(form)
        elif tipo == '3':
            response = crear_archivo_iva_compras(form)
        else:
            response = crear_archivo_alicuotas_compras(form)

        return response


def crear_archivo_iva_ventas(form):
    mes = form.cleaned_data['mes']
    anio = form.cleaned_data['anio']
    tipo = form.cleaned_data['tipo_archivo']
    lista_ventas = []
    response = HttpResponse(content_type='application/text')
    valores_nulos = '000000000000000'  # impuestos y otros tributos
    tipo_cambio = '0001000000'  # tipo de cambio
    valores_nulos3 = '00000000'  # fecha de vencimiento de pago
    alic_iva = '0005'  # 0005 para iva 21 y 0004 para iva 10.5
    alicuotas = '1'  # para factura A y B
    cod_op = '0'
    ventas = Venta.objects.filter(fecha__year=anio, fecha__month=mes).filter(Q(tipo_venta=Tipo.NOTA) | Q(tipo_venta=Tipo.FACTURA)).exclude(tipo_factura='X')
    titulo = 'VENTAS_' + str(mes) + str(anio)
    attachment = 'attachment; filename=' + titulo + '.txt'
    response['Content-Disposition'] = attachment
    for v in ventas:
        fecha = v.fecha.strftime("%Y%m%d")
        tipo_factura = tipo_factura_afip(v, v.tipo_venta, v.tipo_factura)
        nombre = v.cliente.apellido + ', ' + v.cliente.nombre

        articulos = VentaArticulo.objects.filter(venta=v)
        iva10 = 0
        iva21 = 1
        for a in articulos:
            if a.iva == 21:
                iva21 = 1
            else:
                iva10 = 1
        alicuotas = iva10 + iva21

        if len(nombre) <= 30:
            nombre = nombre.ljust(30, " ")
        else:
            nombre = nombre[:30]
        # nombre = nombre.replace('ñ','n')
        # nombre = nombre.decode('utf-8')
        total = str(v.total).split('.')
        iva = str(v.iva).split('.')
        dni_cuit = v.cliente.dni_cuit
        if len(dni_cuit) <= 9:
            cod_dni_cuit = "96"
        else:
            cod_dni_cuit = "80"
        cuit = dni_cuit.replace('-', '')
        cuit = cuit.rjust(20, "0")

        contenido = "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s" % (
            fecha,                              # Fecha (8)
            tipo_factura[0],                    # Tip_comprob(3)
            '00004',                            # Punt_ven(5)
            tipo_factura[1],                    # Número de comprobante(20)
            tipo_factura[1],                    # Número de comprobante_hasta(20)
            cod_dni_cuit,                       # Código de documento (2)
            cuit,                               # Número de identificación (20)
            nombre,                             # Apellido y nombre(30)
            total[0].rjust(13, "0"), total[1],  # Importe total(15)
            valores_nulos,                      # Importe total de conceptos que no integran el precio neto gravado(15)
            valores_nulos,                      # perc_a_no_cat(15)
            valores_nulos,                      # imp_op_exent(15)
            valores_nulos,                      # imp_percep (15)
            valores_nulos,                      # imp_iibb(15)
            valores_nulos,                      # impo_imp_muni(15)
            valores_nulos,                      # Impu_inter(15)
            'PES',                              # Cód_Mone(3)
            tipo_cambio,                        # Tip_camb (10)
            str(alicuotas),                     # Cant_Al_iva (1)
            cod_op,                             # Cód_op(1)
            valores_nulos,                      # Otr_Trib(15)
            valores_nulos3,                     # venc_pago (8)
            "\n")
        lista_ventas.append(dict(c=contenido, fecha=v.fecha))

    for l in lista_ventas:
        response.writelines(l['c'])
    return response

# 001 00002 00000000000000010000 000000000010000 0005 000000000002100
# 001 00002 00000000000000010000 000000000100000 0004 000000000010500
def crear_archivo_alicuotas_ventas(form):
    mes = form.cleaned_data['mes']
    anio = form.cleaned_data['anio']
    tipo = form.cleaned_data['tipo_archivo']
    lista_ventas = []
    response = HttpResponse(content_type='application/text')
    valores_nulos = '000000000000000'  # impuestos y otros tributos
    tipo_cambio = '0001000000'  # tipo de cambio
    valores_nulos3 = '00000000'  # fecha de vencimiento de pago
    alic_iva = '0005'  # 0005 para iva 21 y 0004 para iva 10.5
    alicuotas = '1'  # para factura A y B
    cod_op = '0'
    ventas = Venta.objects.filter(fecha__year=anio, fecha__month=mes).filter(Q(tipo_venta=Tipo.NOTA) | Q(tipo_venta=Tipo.FACTURA)).exclude(tipo_factura='X')
    titulo = 'VENTAS_ALICUOTAS_' + str(mes) + str(anio)
    attachment = 'attachment; filename=' + titulo + '.txt'
    response['Content-Disposition'] = attachment
    for v in ventas:
        fecha = v.fecha.strftime("%Y%m%d")
        tipo_factura = tipo_factura_afip(v, v.tipo_venta, v.tipo_factura)
        nombre = v.cliente.apellido + ', ' + v.cliente.nombre
        if len(nombre) <= 30:
            nombre = nombre.ljust(30, " ")
        else:
            nombre = nombre[:30]
        # nombre = nombre.replace('ñ','n')
        # total = str(ve.neto_gravado).split('.')
        # iva = str(ve.iva).split('.')

        if v.tipo_factura == 'B':
            neto_gravado = v.neto_gravado - v.monto_iva10 - v.monto_iva21
            iva = v.monto_iva10 + v.monto_iva21
        else:
            neto_gravado = Decimal(v.neto_gravado)
            iva = Decimal(v.iva)

        iva10 = v.monto_iva10
        iva21 = v.monto_iva21
        total10 = v.monto_iva10 * Decimal(100.0 / 10.5)
        neto_gravado_10 = total10 - iva10
        neto_gravado_21 = neto_gravado - neto_gravado_10

        if v.monto_iva10 > 0:
            iva10 = "%.2f" % round(iva10, 2)
            total10 = "%.2f" % round(neto_gravado_10, 2)
            iva = str(iva10).split('.')
            neto_gravado_10_arr = str(total10).split('.')

            contenido = "%s%s%s%s%s%s%s%s%s" % (
                tipo_factura[0],                                                # tipo_comprob(3)
                '00004',                                                        # pto_venta(5)
                tipo_factura[1],                                                # nro_factura(20)
                neto_gravado_10_arr[0].rjust(13, "0"), neto_gravado_10_arr[1],  # neto_grabado(15)
                '0004',                                                         # Alic_iva(4) = 0004  iva: 10%
                iva[0].rjust(13, "0"), iva[1],                                  # iva(15)
                "\n")
            lista_ventas.append(dict(c=contenido, fecha=v.fecha))

        if v.monto_iva21 > 0:
            iva21 = "%.2f" % round(iva21, 2)
            total21 = "%.2f" % round(neto_gravado_21, 2)
            iva = str(iva21).split('.')
            neto_gravado_21_arr = str(total21).split('.')

            contenido = "%s%s%s%s%s%s%s%s%s" % (
                tipo_factura[0],                                                # tipo_comprob(3)
                '00004',                                                        # pto_venta(5)
                tipo_factura[1],                                                # nro_factura(20)
                neto_gravado_21_arr[0].rjust(13, "0"), neto_gravado_21_arr[1],  # neto_grabado(15)
                str(alic_iva),                                                  # Alic_iva(4) = 0005  iva: 21%
                iva[0].rjust(13, "0"), iva[1],                                  # iva(15)
                "\n")
            lista_ventas.append(dict(c=contenido, fecha=v.fecha))

    for l in lista_ventas:
        response.writelines(l['c'])
    return response

# Se reemplazo proveedor con cliente
def crear_archivo_iva_compras(form):
    mes = form.cleaned_data['mes']
    anio = form.cleaned_data['anio']
    tipo = form.cleaned_data['tipo_archivo']
    lista_ventas = []
    response = HttpResponse(content_type='application/text')
    valores_nulos = '000000000000000'  # impuestos y otros tributos
    tipo_cambio = '0001000000'  # tipo de cambio
    valores_nulos3 = '00000000'  # fecha de vencimiento de pago
    alic_iva = '0005'  # 0005 para iva 21 y 0004 para iva 10.5
    alicuotas = '1'  # para factura A y B
    cod_op = '0'
    compras = Compra.objects.filter(fecha__year=anio, fecha__month=mes).exclude(tipo_factura='X')
    titulo = 'COMPRAS_' + str(mes) + str(anio)
    attachment = 'attachment; filename=' + titulo + '.txt'
    response['Content-Disposition'] = attachment
    nro_importacion = "                "
    for c in compras:
        fecha = c.fecha.strftime("%Y%m%d")
        # if c.tipo_compra == 'Compra':
        if c.tipo_factura == "A":
            tipo_factura = "001"
        elif c.tipo_factura == "B":
            tipo_factura = "006"
            alicuotas = "0"
        else:
            tipo_factura = "011"
            alicuotas = "0"
        # else:
        #    if c.tipo_factura == "A":
        #        tipo_factura = "003"
        #    elif c.tipo_factura == "B":
        #       tipo_factura = "008"

        # Este el la razon social en el formulario de compras
        nombre = c.cliente.nombre

        if len(nombre) <= 30:
            nombre = nombre.ljust(30, " ")
        else:
            nombre = nombre[:30]

        total = str(c.total).split('.')
        iibb = str(c.percepcion_iibb).split('.')
        cuit = c.cliente.dni_cuit.replace('-', '')
        cuit = cuit.rjust(20, "0")
        pto_venta = str(c.punto_venta).rjust(5, "0")
        numero_compra = c.numero.rjust(20, "0")
        denominacion = ' '
        denominacion = denominacion.ljust(30, " ")
        cod_op = " "

        contenido = "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s" % (
            fecha,                              # Fecha (8)
            tipo_factura,                       # Tip_comprob(3)
            pto_venta,                          # Punt_ven(5)
            numero_compra,                      # Número de comprobante(20)
            nro_importacion,                    # Número de Importacion(16)
            '80',                               # Cód_doc vendedor (2)
            cuit,                               # Núm: identif vededor (20)
            nombre,                             # Apellido y nombre(30)
            total[0].rjust(13, "0"), total[1],  # Importe total(15)
            iibb[0].rjust(13, "0"), iibb[1],    # Importe total de conceptos que no integran el precio neto gravado(15)
            valores_nulos,                      # imp_op_exent(15)
            valores_nulos,                      # imp_perc_a_cuenta_del_IVA(15)
            valores_nulos,                      # impo_imp_muni(15)
            iibb[0].rjust(13, "0"), iibb[1],    # imp_percep (15)
            valores_nulos,                      # Impu_inter(15)
            valores_nulos,                      # imp_iibb(15)
            'PES',                              # Cód_Mone(3)
            tipo_cambio,                        # Tip_camb (10)
            alicuotas,                          # Cant_Al_iva (1)
            cod_op,                             # Cód_op(1)
            valores_nulos,                      # cred_fiscal_computable(15)
            valores_nulos,                      # Otr_Trib(15)
            '00000000000',                      # cuit_emisor (11)
            denominacion,                       # denominacion_emisor(30)
            valores_nulos,                      # iva comision(15)
            "\n")
        lista_ventas.append(dict(c=contenido, fecha=c.fecha))

    for l in lista_ventas:
        response.writelines(l['c'])
    return response


def crear_archivo_alicuotas_compras(form):
    mes = form.cleaned_data['mes']
    anio = form.cleaned_data['anio']
    tipo = form.cleaned_data['tipo_archivo']
    lista_ventas = []
    response = HttpResponse(content_type='application/text')
    valores_nulos = '000000000000000'  # impuestos y otros tributos
    tipo_cambio = '0001000000'  # tipo de cambio
    valores_nulos3 = '00000000'  # fecha de vencimiento de pago
    alic_iva = '0005'  # 0005 para iva 21 y 0004 para iva 10.5
    alicuotas = '1'  # para factura A y B
    cod_op = '0'
    compras = Compra.objects.filter(fecha__year=anio, fecha__month=mes).exclude(tipo_factura='X')
    titulo = 'COMPRAS_ALICUOTAS_' + str(mes) + str(anio)
    attachment = 'attachment; filename=' + titulo + '.txt'
    response['Content-Disposition'] = attachment
    for c in compras:
        fecha = c.fecha.strftime("%Y%m%d")
        if c.tipo_factura == "A":
            tipo_factura = "001"
        elif c.tipo_factura == "B":
            tipo_factura = "006"
        else:
            tipo_factura = "011"
        # Este el la razon social en el formulario de compras
        nombre = c.cliente.nombre
        if len(nombre) <= 30:
            nombre = nombre.ljust(30, " ")
        else:
            nombre = nombre[:30]
        # nombre = nombre.replace('ñ','n')
        if c.iva == 10.5:
            alic_iva = '0004'
        total = c.total
        iibb = c.percepcion_iibb
        total = total - iibb
        total = str(total).split('.')
        iva = str(c.iva).split('.')
        cuit = c.cliente.dni_cuit.replace('-', '')
        cuit = cuit.rjust(20, "0")

        if c.tipo_factura == 'B':
            neto_gravado = c.neto_gravado - c.monto_iva10 - c.monto_iva21
            iva = c.monto_iva10 + c.monto_iva21
        else:
            neto_gravado = Decimal(c.neto_gravado)
            iva = Decimal(c.iva)

        iva10 = c.monto_iva10
        iva21 = c.monto_iva21
        total10 = c.monto_iva10 * Decimal(100.0 / 10.5)
        neto_gravado_10 = total10 - iva10
        neto_gravado_21 = neto_gravado - neto_gravado_10

        if c.monto_iva10 > 0:
            iva10 = "%.2f" % round(iva10, 2)
            total10 = "%.2f" % round(neto_gravado_10, 2)
            iva = str(iva10).split('.')
            neto_gravado_10_arr = str(total10).split('.')

            contenido = "%s%s%s%s%s%s%s%s%s%s%s" % (
                tipo_factura,                                                   # tipo_comprob(3)
                c.punto_venta.rjust(5, "0"),                                    # pto_venta(5)
                c.numero.rjust(20, "0"),                                        # nro_comp(20)
                '80',                                                           # cod_doc(2)
                cuit,                                                           # doc(20)
                neto_gravado_10_arr[0].rjust(13, "0"), neto_gravado_10_arr[1],  # neto(15)
                '0004',                                                         # Alic_iva(4)=0005
                iva[0].rjust(13, "0"), iva[1],                                  # iva(15)
                "\n")

            lista_ventas.append(dict(c=contenido, fecha=c.fecha))

        if c.monto_iva21 > 0:
            iva21 = "%.2f" % round(iva21, 2)
            total21 = "%.2f" % round(neto_gravado_21, 2)
            iva = str(iva21).split('.')
            neto_gravado_21_arr = str(total21).split('.')

            contenido = "%s%s%s%s%s%s%s%s%s%s%s" % (
                tipo_factura,                                                   # tipo_comprob(3)
                c.punto_venta.rjust(5, "0"),                                    # pto_venta(5)
                c.numero.rjust(20, "0"),                                        # nro_comp(20)
                '80',                                                           # cod_doc(2)
                cuit,                                                           # doc(20)
                neto_gravado_21_arr[0].rjust(13, "0"), neto_gravado_21_arr[1],  # neto(15)
                '0005',                                                         # Alic_iva(4)=0005
                iva[0].rjust(13, "0"), iva[1],                                  # iva(15)
                "\n")

            lista_ventas.append(dict(c=contenido, fecha=c.fecha))

    for l in lista_ventas:
        response.writelines(l['c'])
    return response
