# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Max, Q
import select2.models
import select2.fields
from django.dispatch import receiver
from configuracion.views import Tipo
from django.db.models.signals import post_save
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

def numero_formateado_cheque():
    no = Cheque.objects.aggregate(siguiente_numero=Max('id'))
    if no is None:
        return u"CHE-{:08d}".format(1)
    if no['siguiente_numero'] is None:
        return u"CHE-{:08d}".format(1)
    return u"CHE-{:08d}".format(no['siguiente_numero'] + 1)


class ChequeBanco(models.Model):
    nombre = models.CharField(u'Nombre', max_length=150)

    class Meta:
        verbose_name = u'Tipo de Banco'
        verbose_name_plural = u'Tipo de Banco'

    def __unicode__(self):
        return u'{0}'.format(self.nombre)

class TipoCheque:
    UNO = '1'
    DOS = '2'

TIPO_CHEQUE = (
    (TipoCheque.UNO, "1"),
    (TipoCheque.DOS, "2")
)

class ChequePropioOTerceros:
    PROPIO = 'propio'
    TERCEROS = 'terceros'

CHEQUE_PROPIO_O_TERCEROS = (
    (ChequePropioOTerceros.PROPIO, "Propio"),
    (ChequePropioOTerceros.TERCEROS, "Terceros"),
)

class Cheque(models.Model):
    numero = models.CharField(u'numero de cheque', max_length=60, default=numero_formateado_cheque)
    cliente = select2.fields.ForeignKey('cliente.Cliente',
                                        ajax=True,
                                        blank=True, null=True,
                                        search_field=lambda q: Q(nombre__icontains=q) | Q(apellido__icontains=q),
                                        related_name=u'Cliente_cheque',
                                        overlay="...",
                                        js_options={
                                            'quiet_millis': 200,
                                        })
    fecha_entrega = models.DateField(u'Fecha Recibido', blank=True, null=True)
    banco = select2.fields.ForeignKey(ChequeBanco,
                                      ajax=True,
                                      search_field=lambda q: Q(nombre__icontains=q),
                                      related_name=u'tipo_cheque',
                                      overlay="...",
                                      js_options={
                                          'quiet_millis': 200,
                                      })
    modalidad_de_cheque = models.CharField(u'Modalidad de cheque', max_length=40, choices=TIPO_CHEQUE, default=TipoCheque.UNO)
    propio_o_tercero = models.CharField(u'Propio o terceros', max_length=10, choices=CHEQUE_PROPIO_O_TERCEROS, default=ChequePropioOTerceros.PROPIO)
    nro_cheque = models.CharField(u'Nro. de Cheque', max_length=150, )
    cuit = models.CharField(u'CUIT/CUIL', max_length=40, blank=True, null=True)
    importe = models.DecimalField(u'Importe', max_digits=20, decimal_places=2, default=0.00)
    fecha_posdata = models.DateField(u'Fecha Posdata')
    entrega = models.CharField(u'Entregado a:', max_length=100, blank=True, null=True)
    fecha_entregado = models.DateField(u'Fecha entregado', blank=True, null=True)
    descripcion = models.CharField(u'Descripción', max_length=150, blank=True, null=True)
    cobrado = models.BooleanField(u'Cobrado', default=False)

    def __unicode__(self):
        return u'{0}'.format(self.nro_cheque)


class ChequeInline(models.Model):
    cheque = select2.fields.ForeignKey('Cheque',
                                        ajax=True,
                                        search_field=lambda q: Q(numero__icontains=q),
                                        related_name=u'cheque_numero',
                                        overlay="Seleccione un cheque",
                                        limit_choices_to=models.Q(entrega__exact="") | models.Q(entrega__exact=None),
                                        js_options={
                                            'quiet_millis': 200,
                                        })

    limit = models.Q(app_label='venta', model='Venta') | models.Q(app_label='movimientos', model='Movimiento')
    content_type = models.ForeignKey(ContentType, verbose_name=u'Objeto', limit_choices_to=limit, blank=True, null=True )
    object_id = models.PositiveIntegerField(verbose_name='ID')
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u'Cheque inline'
        verbose_name_plural = u'Cheques inline'

@receiver(post_save, sender=ChequeInline)
def chequeinline_post_save(sender, instance, raw, using, update_fields, **kwargs):
    if instance.content_type.model == "movimiento" and (instance.content_object.tipo_venta == Tipo.EGRESO):
        cheque = Cheque.objects.get(id=instance.cheque.id)
        cheque.entrega = instance.content_object.detalle
        cheque.fecha_entregado = instance.content_object.fecha
        cheque.save()
