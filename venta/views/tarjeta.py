# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.http import JsonResponse

from cliente.models import ClienteResumen, Q
from configuracion.views import Tipo, TipoPago
from venta.models import Tarjeta, Cuota

from django.http import JsonResponse

@login_required
def recargo_tarjeta(request, nombre, cuotas):
    recargo = Cuota.objects.select_related('tarjeta').get(tarjeta__nombre=nombre, cuota=cuotas).recargo
    data = {
        'recargo': recargo
    }
    return JsonResponse(data)
