from __future__ import unicode_literals

from django.db import models
from cheque.models import Cheque

class TipoMovimientoLibroBanco:
    INGRESO = 'ingreso'
    EGRESO = 'egreso'

TIPO_MOVIMIENTO_LIBRO_BANCO = (
    (TipoMovimientoLibroBanco.INGRESO, u'Ingreso'),
    (TipoMovimientoLibroBanco.EGRESO, u'Egreso'),
)

class LibroBanco(models.Model):
    class Meta:
        verbose_name_plural = "movimientos"
    monto = models.DecimalField(u'Importe', max_digits=20, decimal_places=2, default=0.00)
    tipo = models.CharField(u'Tipo de Movimiento', max_length=50, choices=TIPO_MOVIMIENTO_LIBRO_BANCO, default=TipoMovimientoLibroBanco.EGRESO)
    fecha = models.DateField(u'Fecha')
    detalle = models.TextField(u'Detalle', blank=True, default=u'')
    cheque = models.ForeignKey(Cheque,
                              blank=True,
                              null=True,
                              on_delete=models.CASCADE
                              )

