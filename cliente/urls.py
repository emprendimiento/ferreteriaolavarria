from django.conf.urls import include, url
from cliente import views

urlpatterns = [
    url(r"^all_clientes/$", views.all_clientes, name='all_clientes'),
    url(r"^all_proveedores/$", views.all_proveedores, name='all_proveedores'),
    url(r"^cargar_clientes/$", views.cargar_clientes, name='cargar_clientes'),
]
