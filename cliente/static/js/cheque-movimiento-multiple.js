django.jQuery(function($) {
	function set_valores_solo_lectura(){
		var max_stock = $('#id_chequemultiplemovimiento_set-TOTAL_FORMS').val();

			for (var x=0; x<max_stock;x++) {
				$('#id_chequemultiplemovimiento_set-'+x+'-importe')
					.attr('readonly','true')
					.attr('type', 'text')
					.attr('style', 'background-color: #fff; ');
					
				$('#id_chequemultiplemovimiento_set-'+x+'-fecha_posdata')
					.attr('readonly','true')
					.attr('type', 'text')
					.attr('style', 'background-color: #fff; ');
				$('#id_chequemultiplemovimiento_set-'+x+'-banco')
					.attr('readonly','true')
					.attr('type', 'text')
					.attr('style', 'background-color: #fff;');
				
			}
	}

	$( document ).ready(function() {
    	//valores de cheuqes
		set_valores_solo_lectura();
		// Para botton agregar
		$('#chequemovimiento_set-group .add-row a').click(function(){ 
			set_valores_solo_lectura();
		});
	});

	function set_valores( importe, fecha, banco,pos) {
		$('#id_chequemultiplemovimiento_set-'+String(pos)+'-banco').val(banco);
		$('#id_chequemultiplemovimiento_set-'+String(pos)+'-importe').val(importe);
		$('#id_chequemultiplemovimiento_set-'+String(pos)+'-fecha_posdata').val(fecha);
	}	
    $('select').on('change', function(d) {
			var id_name = $(this).context.id; 
			var pos = id_name.replace(/[^\d]/g, '');

			var $this = $(this);
			var item_id = $(this).val();
			if ( item_id == '' )
				return;
			$.ajax({
				url: '/admin/cheque_json/' + item_id + '/',
				data: '',
			}).done(function( data ) {
				set_valores(data.importe, data.fecha_posdata, data.banco, pos);
				
			});
		
	 });

})


