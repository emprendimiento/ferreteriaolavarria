# -*- coding: utf-8 -*-
from .settings import *
DEBUG = True
TEMPLATE = {
    'DEBUG': DEBUG,
}



DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ferreteriaolavarria',
        'USER': 'ferreteria',
        'PASSWORD': 'FerreteriaOlavarria123',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

STATIC_ROOT = "/home/static/ferreteriaolavarria"
