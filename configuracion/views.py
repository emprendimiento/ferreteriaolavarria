import csv

from decimal import Decimal

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response
from django.template import RequestContext

# from articulo.models import ArticuloRubro, Articulo
# from cliente.models import Cliente
from configuracion.forms import formActualizarPrecios
# from proveedor.models import Proveedor
class Tipo:
    FACTURA = 'factura'
    REMITO = 'remito'
    COMPRA = 'compra'
    PAGO = 'pago'
    NOTA = 'nota'
    RECIBO = 'recibo'
    EGRESO = 'egreso'
    INGRESO = 'ingreso'
    PRESUPUESTO = 'presupuesto'

TIPO_VENTA = (
    (Tipo.PRESUPUESTO, u'Presupuesto'),
    (Tipo.FACTURA, u'Factura'),
    (Tipo.REMITO, u'Remito'),
    (Tipo.COMPRA, u'Compra'),
    (Tipo.NOTA, u'Nota de Credito'),
    )

TIPO_FACTURA = (
    (u'A', u'A'),
    (u'B', u'B'),
    (u'X', u'X'),
)

class TipoPago:
    EFECTIVO = 'efectivo'
    CUENTA_CORRIENTE = 'cuenta_corriente'
    TARJETA = 'tarjeta'

TIPO_PAGO = (
    (TipoPago.EFECTIVO, u'Efectivo'),
    (TipoPago.CUENTA_CORRIENTE, u'Cuenta Corriente'),
    (TipoPago.TARJETA, u'Tarjeta'),
)

TIPO_MOVIMIENTO = (
    (Tipo.RECIBO, u'Recibo'),
    (Tipo.INGRESO, u'Ingreso'),
    (Tipo.EGRESO, u'Egreso'),
    (Tipo.PAGO, u'Pago'),
)


MOV = (
    (Tipo.INGRESO, u'Ingreso'),
    (Tipo.EGRESO, u'Egreso'),
    
)

TIPO_NOTIFICACION = (
    (Tipo.FACTURA, u'Factura'),
    (Tipo.REMITO, u'Remito'),
    (Tipo.COMPRA, u'Compra'),
    (Tipo.PAGO, u'Pago'),
    (Tipo.NOTA, u'Nota de Credito'),
    (Tipo.RECIBO, u'Recibo'),
    (Tipo.INGRESO, u'Ingreso'),
    (Tipo.EGRESO, u'Egreso'),
)

CONFIRMACION = (
    (u'SI', u'SI'),
    (u'NO', u'NO'),
)

@login_required
def cargar_rubros(request):
    with open('private/rubroExportados.csv','rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
             nombre = row[1].decode('latin-1')
             rubro = 'articulo.ArticuloRubro'(id=int(row[0]), nombre=nombre)
             rubro.save()
    # return render_to_response(template, context_instance=RequestContext(request, locals()))

@login_required
def cargar_proveedores(request):
    with open('private/proveedoresMundoW.csv','rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for row in spamreader:
            print row
            direccion = row[2].decode('latin-1')
            nombre = row[1].decode('latin-1')
            localidad = row[3].decode('latin-1')
            if not row[4] == "":
                cp = row[4]
            else:
                cp=None
            proveedor = 'proveedor.Proveedor'(id=int(row[0]),
                                    razon_social=nombre,
                                    cuit=row[6],
                                    localidad=localidad,
                                    direccion=direccion,
                                    telefono=row[5],
                                    codigo_postal=cp,
                                    codigo=nombre.upper()[:3]
                                    )
            proveedor.save()
    #return render_to_response(template, context_instance=RequestContext(request, locals()))


    #return render_to_response(template, context_instance=RequestContext(request, locals()))
