# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Movimiento, IngresoEgreso
from reversion.admin import VersionAdmin
from configuracion.views import MOV
from django.forms import ModelForm

from django.contrib.contenttypes.admin import GenericTabularInline
from cheque.admin import ChequeInlineAdmin

'''
class MovimientoForm(ModelForm):
    class Meta:
         model = Movimiento
         exclude = []
    def __init__(self, *args, **kwargs):
        super(MovimientoForm, self).__init__(*args, **kwargs)
        if self.fields['tipo_venta']:
'''

class AdminMovimiento(admin.ModelAdmin):

    readonly_fields = ['numero',]
    fieldsets = (
        (None, {
            'fields': (
                ('numero','tipo_venta','fecha'),
                'cliente',
                'detalle',
                'total',
            )}),
    )
    list_display = [
        'cliente',
        'num',
        'tipo_venta',
        'fecha',
        'detalle',
        'total',
    ]
    ordering = (
        '-fecha',
        'cliente__apellido',
        'detalle',
    )
    list_filter = ('cliente__tipo',)
    search_fields = ['cliente__nombre',
                     'cliente__apellido',
                  'cliente__dni_cuit',
    ]

    inlines = [
        ChequeInlineAdmin,
    ]

    class Media:
        js = (
            'js/cheques.js',
        )
        css = {
            'all': ('css/base.css',)
        }

    def has_add_permission(self, request):
        return False

    def num(self, obj):
        if obj.numero:
            return "{:05d}".format(int(obj.numero))

    num.short_description = 'Número'

class IngresoEgresoAdmin(AdminMovimiento):
    fieldsets = (
        (None, {
            'fields': (
                ('numero','tipo_venta','fecha'),
                'detalle',
                'total',
            )}),
    )
    inlines = [
        ChequeInlineAdmin,
    ]

    class Media:
        js = (
            'js/cheques.js',
        )
        css = {
            'all': ('css/base.css',)
        }

    def has_add_permission(self, request):
        return True

    def get_form(self, request, obj=None, **kwargs):
        form = super(IngresoEgresoAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['tipo_venta'].choices = MOV
        return form


admin.site.register(Movimiento, AdminMovimiento)
admin.site.register(IngresoEgreso, IngresoEgresoAdmin)
