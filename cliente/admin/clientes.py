import datetime
from django.contrib import admin
from django.http import HttpResponseRedirect
from easy_maps.widgets import AddressWithMapWidget
from easy_pdf.rendering import render_to_pdf_response
from reversion.admin import VersionAdmin
from copy import deepcopy
from django import forms
from cliente.models import Cliente, TipoCliente, Vendedor


class FormGoogle(forms.ModelForm):
    class Meta:
        widgets = {
            'autorizacion': forms.RadioSelect,
            'ubicacion': AddressWithMapWidget({'class': 'vTextField'}),
        }

class ClienteAdmin(VersionAdmin):
    form = FormGoogle
    fieldsets = (
        (None, {
            'fields': ('nombre',
                       'apellido',
                       'dni_cuit',
                       'tipo_factura',
                       ('provincia', 'localidad', 'direccion', 'codigo_postal'),
                       'ubicacion',
                       'email',
                       'telefono',
                       'observaciones'
                       )
        }),
    )
    search_fields = [
        'apellido',
        'nombre',
        'dni_cuit',
    ]
    list_display = [
        'nombre',
        'apellido',
        'dni_cuit',
        'email',
        'telefono',
    ]
    ordering = (
        'apellido',
        'nombre',
    )
    class Media:
        js = (
            'js/ubicaciones.js',
        )


    def get_queryset(self, request):
        return super(ClienteAdmin, self).get_queryset(request).exclude(tipo=TipoCliente.PROVEEDOR)

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Vendedor)
