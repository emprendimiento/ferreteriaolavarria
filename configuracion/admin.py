from django.contrib import admin

# configuracion
from configuracion.models import Configuracion, Propietario


class ConfiguracionAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('presupuesto',
                       ('facturaX'),
                       ( 'nota_creditoX'),
                       ('remito', 'recibo', 'pago', 'egreso', 'ingreso'),
                       'punto_venta',
                       'url_wsaa',
                       'url_factura',
                       'url_cache_local',
                       'url_estaticos',
                       ('nombre_crt','nombre_key'),
                       'web_services',
                       ),

        }),
    )

admin.site.register(Configuracion, ConfiguracionAdmin)
admin.site.register(Propietario)
